import random
import re
from datetime import datetime

from robbiblubber.util.coding.invertedbase62 import InvertedBase62
from robbiblubber.util.collectionop import CollectionOp
from robbiblubber.util.stringbuilder import StringBuilder
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class provides string operation methods. ##
class StringOp:
    ############################################################################
    # static methods                                                           #
    ############################################################################

    ## Returns if a value is NONE or a string that only contains whitespaces.
    # @param s String.
    # @return Returns TRUE if the value is NONE or whitespace, otherwise returns FALSE. ##
    @staticmethod
    def isNullOrWhitespace(s: str) -> bool:
        try:
            if s is None: return True
            if s.strip() == '': return True
        except:
            pass
        return False


    ## Returns a random alphanumeric string of a given length from an alphabet.
    # @param length Random string length.
    # @param alphabet Alphabet.
    # @return Random string. ##
    @staticmethod
    def random(length: int = 24, alphabet: str = InvertedBase62.ALPHABET) -> str:
        rval = StringBuilder()
        l = len(alphabet)

        for i in range(length):
            rval.append(alphabet[random.randrange(0, l)])

        return rval.toString()


    ## Returns a unique random alphanumeric string of a given length.
    # @param String length.
    # @return Unique string. ##
    @staticmethod
    def unique(length: int = 24) -> str:
        return (StringOp.random(3) + StringOp.toAlpha((datetime.utcnow() - datetime(1, 1, 1)).total_seconds() * 10000000 + datetime.utcnow().microsecond) + StringOp.random(length))[0:length]


    ## Converts an integer to a signed string of a given base.
    # @param value Integer.
    # @param base Base.
    # @param alphabet Alphabet.
    # @return String. ##
    @staticmethod
    def toString(value: int, base: int, alphabet: str = InvertedBase62.ALPHABET) -> str:
        neg = False
        rval = StringBuilder()

        if (base < 2) or (base > len(alphabet)): raise ValueError('Invalid base.')

        if value < 0:
            neg = True
            value = -value

        while value > 0:
            rval.insert(0, alphabet[int(value % base)])
            value = int(value / base)

        if neg: rval.insert(0, '-')

        return rval.toString()


    ## Converts a long integer to an alphanumeric string.
    # @param value Value.
    # @return Alphanumeric string. ##
    @staticmethod
    def toAlpha(value: int) -> str:
        return StringOp.toString(value, 62)


    ## Converts a string of a given base to an integer.
    # @param value String.
    # @param base Base.
    # @param alphabet Alphabet.
    # @return Integer. ##
    @staticmethod
    def toInteger(value: str, base: int = 10, alphabet: str = InvertedBase62.ALPHABET) -> int:
        neg = False
        rval = 0

        if (base < 2) or (base > len(alphabet)): raise ValueError('Invalid base.')

        value = value.strip()
        if value[0] == '-':
            neg = True
            value = value[1:]

        l = len(value) - 1
        for i in range(len(value)):
            rval += pow(base, i) * alphabet.index(value[l - i])

        return -rval if neg else rval


    ## Returns a boolean representation of the string value.
    # @param s String.
    # @return Boolean. ##
    @staticmethod
    def toBoolean(s: str) -> bool:
        if s is None: return False
        try:
            return int(s) != 0
        except: pass

        return ((s.lower()[0] == 't') or (s.lower()[0] == 'y'))


    ## Returns a string representation of the Boolean value.
    # @param b Boolean.
    # @return String. ##
    @staticmethod
    def toBooleanString(b: bool) -> str:
        return 'true' if b else 'false'


    ## Returns the string part before the separator.
    # @param s String.
    # @param separator Separator.
    # @return String part preceding the separator expression. ##
    @staticmethod
    def before(s: str, separator: str) -> str:
        try:
            return s[:s.index(separator)]
        except: pass

        return s


    ## Returns the string part after the separator.
    # @param s String.
    # @param separator Separator.
    # @return String part succeding the separator expression. ##
    @staticmethod
    def after(s: str, separator: str) -> str:
        try:
            return s[s.index(separator) + len(separator):]
        except: pass

        return ''


    ## Gets the number of identical characters at the start of two strings.
    # @param expression Expression.
    # @param pattern Pattern.
    # @param caseSensitive Determines if the match is case-sensitive.
    # @return Returns the number of matching characters. ##
    @staticmethod
    def matchesFromStart(expression: str, pattern: str, caseSensitive: bool = True) -> int:
        if not caseSensitive:
            expression = expression.lower()
            pattern = pattern.lower()

        i = 0
        while i < min(len(expression), len(pattern)):
            if expression[i] != pattern[i]: return i;
            i += 1

        return i


    ## Checks if an expression matches a pattern.
    # @param Expression.
    # @param One or more aptterns.
    # @param caseSensitive Determines if the match is case-sensitive.
    # @return Returns TRUE if the pattern matches, otherwise returns FALSE. ##
    @staticmethod
    def matches(expression: str, pattern, caseSensitive: bool = True) -> bool:
        if pattern is None: return True

        if CollectionOp.isIterable(pattern):
            for i in pattern:
                if StringOp.matches(expression, i, caseSensitive): return True
            return False

        pattern = '^' + re.escape(pattern).replace('\\*', '.*').replace('\\?', '.') + '$'
        return bool(re.findall(pattern, expression, 0 if caseSensitive else re.I))


    ## Returns the maximum length of the strings in an array.
    # @param s... Strings.
    # @return Length of the longest string in the array. ##
    @staticmethod
    def strLen(*args) -> int:
        rval = 0
        for i in args:
            if CollectionOp.isIterable(i):
                for k in i:
                    if len(k) > rval: rval = len(k)
            elif len(i) > rval: rval = len(i)

        return rval


    ## Returns if one of the passed strings is NONE.
    # @param s... Strings.
    # @return Returns TRUE if a string is NONE, otherwise returns FALSE. ##
    @staticmethod
    def anyNull(*args) -> bool:
        for i in args:
            if i is None: return True
            if CollectionOp.isIterable(i):
                for k in i:
                    if k is None: return True

        return False


    ## Returns a string in which all occurances of a character sequence are replaced.
    # @param s String.
    # @param oldv Old value.
    # @param newv New value.
    # @param caseSensitive Determines if the replace operation should be case-sensitive.
    # @return String with replacements. ##
    @staticmethod
    def replaceAll(s: str, oldv: str, newv: str, caseSensitive: bool = True):
        if StringOp.anyNull(s, oldv, newv): return s

        if caseSensitive:
            return s.replace(oldv, newv)

        while True:
            try:
                v = s.lower().index(oldv.lower())
                s = s[:v] + newv + s[v + len(oldv):]
            except:
                return s


    ## Returns a string in which the first occurance of a character sequence is replaced.
    # @param s String.
    # @param oldv Old value.
    # @param newv New value.
    # @param caseSensitive Determines if the replace operation should be case-sensitive.
    # @return String with replacements. ##
    @staticmethod
    def replaceFirst(s: str, oldv: str, newv: str, caseSensitive: bool = True):
        if StringOp.anyNull(s, oldv, newv): return s

        if caseSensitive:
            return s.replace(oldv, newv, 1)

        try:
            v = s.lower().index(oldv.lower())
            s = s[:v] + newv + s[v + len(oldv):]
        except: pass

        return s


    ## Gets the part of the string before the last occurance of a character sequence.
    # @param s String.
    # @param c Sequence.
    # @return String part. ##
    @staticmethod
    def beforeLast(s: str, c: str) -> str:
        try:
            return s[:s.rindex(c)]
        except: pass

        return s


    ## Gets the part of the string before the first occurance of a character sequence.
    # @param s String.
    # @param c Sequence.
    # @return String part. ##
    @staticmethod
    def beforeFirst(s: str, c: str) -> str:
        try:
            return s[:s.index(c)]
        except: pass

        return  s


    ## Gets the part of the string after the last occurance of a character sequence.
    # @param s String.
    # @param c Sequence.
    # @return String part. ##
    @staticmethod
    def afterLast(s: str, c: str) -> str:
        try:
            return s[s.rindex(c) + len(c):]
        except:
            pass

        return s


    ## Gets the part of the string after the first occurance of a character sequence.
    # @param s String.
    # @param c Sequence.
    # @return String part. ##
    @staticmethod
    def afterFirst(s: str, c: str) -> str:
        try:
            return s[s.index(c) + len(c):]
        except:
            pass

        return s


    ## Returns if a string complies to a given pattern.
    # @param s String.
    # @param pattern Pattern.
    # @return Returns TRUE if the string complies, otherwise returns FALSE. ##
    @staticmethod
    def complies(s: str, pattern: str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') -> bool:
        for i in s:
            if not i in pattern: return False

        return True


    ## Gets all characters contained in a string.
    # @param s String.
    # @return A string containing each character used in the string once. ##
    @staticmethod
    def getChars(s: str) -> str:
        rval = []
        for i in s:
            if not i in rval: rval.append(i)
        rval.sort()

        return ''.join(rval)


    ## Returns a value or default value if value is null.
    # @param s Value.
    # @param defaultValue Default value.
    # @return Value. ##
    @staticmethod
    def nvl(s, defaultValue = ''):
        if s is None: return defaultValue
        return s


    ## Replaces a string part at the beginning of a string.
    # @param s String.
    # @param find String to replace.
    # @param replace String to replace witth.
    # @param caseSensitive Determines if the replace operation should be case-sensitive.
    # @return String. ##
    @staticmethod
    def replaceStart(s: str, find: str, replace: str, caseSensitive: bool = True) -> str:
        n = 0
        while (s.startswith(find) or ((not caseSensitive) and s.lower().startswith(find.lower()))):
            n += 1
            s = s[len(find):]

        return (replace * n) + s


    ## Replaces a string part at the end of a string.
    # @param s String.
    # @param find String to replace.
    # @param replace String to replace witth.
    # @param caseSensitive Determines if the replace operation should be case-sensitive.
    # @return String. ##
    @staticmethod
    def replaceEnd(s: str, find: str, replace: str, caseSensitive: bool = True) -> str:
        n = 0
        while (s.endswith(find) or ((not caseSensitive) and s.lower().endswith(find.lower()))):
            n += 1
            s = s[:len(s) - len(find)]

        return s + (replace * n)


    ## Returns the index of the first occurance of the given character sequences.
    # @param s String.
    # @param c Array of search strings.
    # @param startIndex Start index.
    # @return Returns the index of the first occurance of a search string or -1 if none occurs. ##
    @staticmethod
    def indexOfAny(s: str, c: [], startIndex: int = 0) -> int:
        rval = -1
        for i in c:
            try:
                ci = s.index(i, startIndex)
                if (rval < 1) or (ci < rval): rval = ci
            except: pass

        return rval


    ## Returns a part of the string between the m-th occurence of a character (from) and the n-th occurrence of a
    #  string (to) or the end of the string (to is omitted or 0).
    # @param s String.
    # @param frm Start occurance.
    # @param to End occurance.
    # @param delimiters Delimiter characters.
    # @return String sniplet. ##
    @staticmethod
    def snip(s: str, frm: int = 1, to: int = 0, delimiters: [] = (' ')) -> str:
        start = end = -1

        if to == 0: to = -2
        for i in range(frm):
            start = StringOp.indexOfAny(s, delimiters, start + 1)
            if start < 0: return ''

        if to < frm: return s[start + 1:]
        if to == frm: return ''

        for i in range(to):
            end = StringOp.indexOfAny(s, delimiters, end + 1)
            if end < 1: return s[start + 1:]

        return s[start + 1:end]


    ## Returns a list of strings as a chained string.
    # @param strings String array.
    # @param separator Separator.
    # @return Chained string. ##
    @staticmethod
    def chain(strings: [], separator: str = ',') -> str:
        if (separator is None) or (separator == ''): return ''.join(strings)

        rval = StringBuilder()
        first = True

        for i in strings:
            if first:
                first = False
            else:
                rval.append(separator)
            rval.append(i)

        return rval.toString()


    ## Returns if a string is numeric.
    # @param s String.
    # @return Returns TRUE if the string is numeric, otherwise returns FALSE. ##
    @staticmethod
    def isNumeric(s: str):
        try:
            _ = float(s)
            return True
        except: pass

        return False


    ## Returns if a string is a numeric integer.
    # @param s String.
    # @return Returns TRUE if the string is a numeric integer, otherwise returns FALSE. ##
    @staticmethod
    def isNumericInteger(s: str):
        try:
            _ = int(s)
            return True
        except:
            pass

        return False
