
from ftplib import FTP

from robbiblubber.util.stringbuilder import StringBuilder

import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class provides FTP support for ddp downloads. ##
class _FtpUtility:
    ############################################################################
    # constructor                                                              #
    ############################################################################

    ## Creates a new instance of this class. ##
    def __init__(self):
        self._str = StringBuilder()
        self._first = True



    ############################################################################
    # static methods                                                           #
    ############################################################################

    ## Gets the contents of an FTP file.
    # @param url URL.
    # @param user User name.
    # @param password Password.
    # @param encoding Encoding.
    # @return File contens as a string. ##
    @staticmethod
    def getFtpContents(url: str, user: str = None, password = None, encoding: str = 'utf-8'):
        if(url.lower().startswith('ftp://')): url = url[6:]
        p = url.split('/')
        s = _FtpUtility()

        ftp = FTP(p[0]) if user is None else FTP(p[0], user, password)

        ftp.encoding = encoding
        for i in range(len(p) - 2):
            ftp.cwd(p[i + 1])
        srec = s._rec
        ftp.retrlines('RETR ' + p[len(p) - 1], srec)
        ftp.close()

        return s._str.toString()



    ############################################################################
    # methods                                                                  #
    ############################################################################

    ## Receives a string.
    # @param s String. ##
    def _rec(self, s: str):
        if self._first:
            self._first = False
        else: self._str.append('\r\n')

        self._str.append(s)
