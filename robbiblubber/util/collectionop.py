
from collections.abc import Iterable
import random as rnd

from robbiblubber.util.collections.imutablelist import IMutableList
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

class CollectionOp:
    ############################################################################
    # static methods                                                           #
    ############################################################################

    ## Returns if an object is iterable.
    # @param c Collection.
    # @return Returns TRUE if the object is iterable, otherwise returns FALSE.
    # @remarks Returns FALSE for strings. ##
    @staticmethod
    def isIterable(c) -> bool:
        if isinstance(c, str): return False
        if isinstance(c, Iterable): return True
        return False


    ## Returns a random item from the collection.
    # @param c Collection.
    # @param count Number of items to pick.
    # @return Random element. ##
    @staticmethod
    def random(c: [], count: int = 0):
        if not isinstance(c, list): c = list(c)
        if count == 0: return rnd.choice(c)

        return rnd.sample(c, count)


    ## Returns a shuffled collection.
    # @param c Collection.
    # @return Shuffled list. ##
    @staticmethod
    def shuffled(c: []) -> []:
        if isinstance(c, IMutableList):
            c = c._items
        elif not isinstance(c, list):
            c = list(c)

        return rnd.sample(c, len(c))


    ## Shuffles a collection.
    # @param c Collection.
    # @return Returns the shuffled object. ##
    @staticmethod
    def shuffle(c: []) -> []:
        if isinstance(c, IMutableList):
            rnd.shuffle(c._items)
        else:
            rnd.shuffle(c)

        return c

