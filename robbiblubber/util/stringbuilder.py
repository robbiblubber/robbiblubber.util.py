
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class implements a string builder. ##
class StringBuilder:
    ############################################################################
    # constructor                                                              #
    ############################################################################

    ## Creates a new instance of this class. ##
    def __init__(self):
        self._parts = []



    ############################################################################
    # magic methods                                                            #
    ############################################################################

    ## Returns a string representation of this instance. ##
    def __str__(self):
        return self.toString()



    ############################################################################
    # methods                                                                  #
    ############################################################################

    ## Appends a string to the string builder.
    # @param s String. ##
    def append(self, s: str):
        if not ((s is None) or (s == '')): self._parts.append(s)


    ## Inserts a string into the string builder.
    # @param pos Position.
    # @param s String. ##
    def insert(self, pos: int, s: str):
        if not ((s is None) or (s == '')): self._parts.insert(pos, s)


    ## Returns a string representation of this instance. ##
    def toString(self):
        return ''.join(self._parts)
    