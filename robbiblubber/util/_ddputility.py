from collections.abc import Iterable
from datetime import datetime

from robbiblubber.util._ddpdefault import _DdpDefault
from robbiblubber.util._ddpline import _DdpLine
from robbiblubber.util.stringbuilder import StringBuilder
from robbiblubber.util.stringrdr import StringRdr
from robbiblubber.util._ddpcomment import _DdpComment
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class provides ddp utility methods. ##
class _DdpUtility:
    ############################################################################
    # constants                                                                #
    ############################################################################

    ## String data type. ##
    STR = 0

    ## Integer data type. ##
    INT = 1

    ## Float data type. ##
    FLOAT = 2

    ## Date/time data type. ##
    DATETIME = 4

    ## String array data type. ##
    STR_ARRAY = 128

    ## Int array data type. ##
    INT_ARRAY = 129

    ## Float array data type. ##
    FLOAT_ARRAY = 130

    ## Float array data type. ##
    DATETIME_ARRAY = 132

    ## Internal representation for the backslash character. ##
    BACKSLASH = chr(0x1) + chr(0x16) + chr(0x16) + chr(0x16) + chr(0x11) + chr(0x1d)

    ## Internal representation for the slash character. ##
    SLASH = chr(0x1) + chr(0x16) + chr(0x16) + chr(0x16) + chr(0x12) + chr(0x1d)

    ## Internal representation for the equals character. ##
    EQUALS = chr(0x1) + chr(0x16) + chr(0x16) + chr(0x16) + chr(0x13) + chr(0x1d)

    ## Internal representation for the semicolon character. ##
    SEMICOLON = chr(0x1) + chr(0x16) + chr(0x16) + chr(0x16) + chr(0x14) + chr(0x1d)

    ## Internal representation for the line break character. ##
    LINEBREAK = chr(0x1) + chr(0x16) + chr(0x16) + chr(0x16) + chr(0x1f) + chr(0x1d)

    ## Internal representation for the comma character. ##
    COMMA = chr(0x1) + chr(0x16) + chr(0x16) + chr(0x16) + chr(0xe) + chr(0x1d)

    ## Internal representation for the space character. ##
    SPACE = chr(0x1) + chr(0x16) + chr(0x16) + chr(0x16) + chr(0x1) + chr(0x1d)

    ## Internal representation for the tab character. ##
    TAB = chr(0x1) + chr(0x16) + chr(0x16) + chr(0x16) + chr(0x5) + chr(0x1d)

    ## Internal representation for the open bracket character. ##
    BRACKET_OPEN = chr(0x1) + chr(0x16) + chr(0x16) + chr(0x16) + chr(0x2) + chr(0x1d)

    ## Internal representation for the closed bracket character. ##
    BRACKET_CLOSED = chr(0x1) + chr(0x16) + chr(0x16) + chr(0x16) + chr(0x3) + chr(0x1d)



    ############################################################################
    # methods                                                                  #
    ############################################################################

    ## Reads and returns comment sections.
    # @param re String reader.
    # @param multiLine Determines if the comment is multi-line.
    # @return Comment text. ##
    @staticmethod
    def _readOn(re: StringRdr, multiLine: bool) -> str:
        rval = StringBuilder()

        re.read()
        while (re.read()):
            if multiLine:
                if (re.current == '*') and (re.next == '/'):
                    re.read()
                    return rval.toString()
                else:
                    rval.append(re.current)
            elif re.current == '\n':
                return rval.toString()
            else:
                rval.append(re.current)

        return rval.toString()


    ## Masks leading and tailing spaces in a string.
    # @param s String.
    # @param mask Mask string.
    # @return Masked string. ##
    @staticmethod
    def _maskPads(s: str, mask: str) -> str:
        pad = ''
        while (len(s) > 0) and (s[0] == ' '):
            pad += mask
            s = s[1:]
        s = (pad + s)

        pad = ''
        while (len(s) > 0) and (s[len(s) - 1] == ' '):
            pad += mask
            s = s[:(len(s) - 1)]
        s += pad

        return s


    ## Parses a type.
    # @param t Type.
    # @return Type constant. ##
    @staticmethod
    def parseType(t) -> int:
        if type(t) == int: return t
        if t == str: return _DdpUtility.STR
        if t == int: return _DdpUtility.INT
        if t == float: return _DdpUtility.FLOAT
        if t == datetime: return _DdpUtility.DATETIME
        if t == Iterable: return _DdpUtility.STR_ARRAY
        if t == []: return _DdpUtility.STR_ARRAY
        if t == [str]: return _DdpUtility.STR_ARRAY
        if t == [int]: return _DdpUtility.INT_ARRAY
        if t == [float]: return _DdpUtility.FLOAT_ARRAY
        if t == [datetime]: return _DdpUtility.DATETIME_ARRAY
        return _DdpUtility.STR


    ## Gets a default value.
    # @param currentDefault Current default value.
    # @param absDefault Default value for the type.
    # @return Returns the current default if set, otherwise returns the type default. ##
    @staticmethod
    def getDefault(currentDefault, absDefault):
        if isinstance(currentDefault, _DdpDefault): return absDefault
        return currentDefault


    ## Masks special characters in a string.
    # @param s String.
    # @return Masked string. ##
    @staticmethod
    def maskSpecialChars(s: str) -> str:
        if s is None: return ''

        s = s.replace('\\', '\\\\').replace('/', '\\/').replace('=', '\\=').replace(';', '\\;').replace(',', '\\,') \
            .replace('{', '\\{').replace('}', '\\}').replace('\r\n', '\n').replace('\n', '\\n').replace('\t', '\\t')

        return _DdpUtility._maskPads(s, '\\_')


    ## Unmasks special characters in a string.
    # @param s Masked string.
    # @return String. ##
    @staticmethod
    def unmaskSpecialChars(s: str) -> str:
        if s is None: return ''

        return s.replace('\\/', '/').replace('\\=', '=').replace('\\;', ';').replace('\\,', ',').replace('\\{', '{').replace('\\}', '}') \
            .replace('\\n', '\r\n').replace('\\t', '\t').replace('\\_', ' ').replace('\\\\', '\\') \
            .replace(_DdpUtility.SLASH, '/').replace(_DdpUtility.EQUALS, '=').replace(_DdpUtility.SEMICOLON, ';') \
            .replace(_DdpUtility.COMMA, ';').replace(_DdpUtility.BRACKET_OPEN, '{').replace(_DdpUtility.BRACKET_CLOSED, '}') \
            .replace(_DdpUtility.LINEBREAK, '\r\n').replace(_DdpUtility.SPACE, ' ').replace(_DdpUtility.TAB, '\t').replace(_DdpUtility.BACKSLASH, '\\')


    ## Applies internal representation of special characters on a string.
    # @param s String.
    # @param masked Determines if the string is already masked.
    # @return Masked string. ##
    @staticmethod
    def applyInternal(s: str, masked: bool = False) -> str:
        if s is None: return ''

        if masked:
            s = s.replace('\\\\', _DdpUtility.BACKSLASH).replace('\\/', _DdpUtility.SLASH).replace('\\=',
                                                                                                   _DdpUtility.EQUALS) \
                .replace('\\;', _DdpUtility.SEMICOLON).replace('\\,', _DdpUtility.COMMA) \
                .replace('\\{', _DdpUtility.BRACKET_OPEN).replace('\\}', _DdpUtility.BRACKET_CLOSED).replace('\\n',
                                                                                                             _DdpUtility.LINEBREAK) \
                .replace('\\t', _DdpUtility.TAB).replace('\\_', _DdpUtility.SPACE)
        else:
            s = s.replace('\r\n', '\n').replace('\\', _DdpUtility.BACKSLASH).replace('/', _DdpUtility.SLASH).replace(
                '=', _DdpUtility.EQUALS) \
                .replace(';', _DdpUtility.SEMICOLON).replace(',', _DdpUtility.COMMA) \
                .replace('{', _DdpUtility.BRACKET_OPEN).replace('}', _DdpUtility.BRACKET_CLOSED).replace('\n',
                                                                                                         _DdpUtility.LINEBREAK) \
                .replace('\t', _DdpUtility.TAB)

        return _DdpUtility._maskPads(s, _DdpUtility.SPACE)


    ## Converts internally masked characters to masked characters.
    # @param s Internally masked string.
    # @return Masked string. ##
    @staticmethod
    def internalToMask(s: str) -> str:
        if s is None: return None

        return s.replace(_DdpUtility.BACKSLASH, '\\\\').replace(_DdpUtility.SLASH, '\\/').replace(_DdpUtility.EQUALS,
                                                                                                  '\\=') \
            .replace(_DdpUtility.SEMICOLON, '\\;').replace(_DdpUtility.COMMA, '\\,') \
            .replace(_DdpUtility.BRACKET_OPEN, '\\{').replace(_DdpUtility.BRACKET_CLOSED, '\\}') \
            .replace(_DdpUtility.LINEBREAK, '\\n').replace(_DdpUtility.TAB, '\\t').replace(_DdpUtility.SPACE, '\\_')


    ## Parses a path expression.
    # @param path Path.
    # @return List of fully masked path elements. ##
    @staticmethod
    def parsePath(path: str) -> []:
        path = _DdpUtility.applyInternal(path, True)
        while '//' in path: path = path.replace('//', '/')
        return path.split('/')


    ## Reads a line from stream.
    # @param re String reader
    # @param comment Comment.
    # @return Line object. ##
    @staticmethod
    def readLine(re: StringRdr, comment: _DdpComment):
        term = _DdpLine.EOF
        rval = StringBuilder()

        while re.read():
            if re.current == '\\':
                dat = {'\\': _DdpUtility.BACKSLASH, ';': _DdpUtility.SEMICOLON, ',': _DdpUtility.COMMA,
                       '/': _DdpUtility.SLASH, '=': _DdpUtility.EQUALS, 'n': _DdpUtility.LINEBREAK,
                       't': _DdpUtility.TAB, '_': _DdpUtility.SPACE,
                       '{': _DdpUtility.BRACKET_OPEN, ']': _DdpUtility.BRACKET_CLOSED}
                rval.append(dat.get(re.next, re.next))
            elif re.current == '/':
                c = None
                if re.next == '/':
                    c = _DdpUtility._readOn(re, False)
                elif re.next == '*':
                    c = _DdpUtility._readOn(re, True)
                else:
                    rval.append('/')

                if c is not None:
                    if comment.value != '': comment.value += '\r\n'
                    comment.value += c
            elif re.current == ';':
                term = _DdpLine.SEMICOLON
                break
            elif re.current == '{':
                term = _DdpLine.BRACKET_OPEN
                break
            elif re.current == '}':
                term = _DdpLine.BRACKET_CLOSE
                break
            else:
                rval.append(re.current)

        return _DdpLine(rval.toString(), term)
