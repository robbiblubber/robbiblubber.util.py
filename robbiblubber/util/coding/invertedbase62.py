
from robbiblubber.util.coding.basen import BaseN
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class provides Base62 support. ##
class InvertedBase62(BaseN):
    ############################################################################
    # constants                                                                #
    ############################################################################

    ## The hexadecimal alphabet. ##
    ALPHABET: str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

    ## Instance. ##
    _instance = None



    ############################################################################
    # constructor                                                              #
    ############################################################################

    ## Creates a new instance of this class.
    # @param encoding Encoding.
    # @param alphabet Alphabet. ##
    def __init__(self, encoding: str = "utf-8", alphabet: str = None):
        if alphabet is None: alphabet = InvertedBase62.ALPHABET
        super().__init__(62, alphabet, encoding)



    ############################################################################
    # static methods                                                           #
    ############################################################################

    ## Gets the singleton instance.
    # @return Instance. ##
    @staticmethod
    def getInstance():
        if (InvertedBase62._instance is None):
            InvertedBase62._instance = InvertedBase62()
        return InvertedBase62._instance


    ## Converts bytes to a hexadecimal string.
    # @param value Bytes.
    # return Encoded string. ##
    @staticmethod
    def fromBytes(value: bytearray) -> str:
        return InvertedBase62.getInstance().encodeBytes(value)


    ## Converts a string to a hexadecimal string.
    # @param value String.
    # return Encoded string. ##
    @staticmethod
    def fromString(value: str) -> str:
        return InvertedBase62.getInstance().encodeString(value)


    ## Converts an integer to a hexadecimal string.
    # @param value Integer.
    # return Encoded string. ##
    @staticmethod
    def fromInt(value: int) -> str:
        return InvertedBase62.getInstance().encodeInt(value)


    ## Converts a double to a hexadecimal string.
    # @param value Double.
    # return Encoded string. ##
    @staticmethod
    def fromDouble(value: float) -> str:
        return InvertedBase62.getInstance().encodeDouble(value)


    ## Decodes a string to a byte array.
    # @param s Encoded string.
    # return Byte array. ##
    @staticmethod
    def toBytes(s: str) -> bytearray:
        return InvertedBase62.getInstance().decodeToBytes(s)


    ## Decodes a string to a string.
    # @param s Encoded string.
    # return String. ##
    @staticmethod
    def toString(s: str) -> str:
        return InvertedBase62.getInstance().decodeToString(s)


    ## Decodes a string to an integer.
    # @param s Encoded string.
    # return Integer. ##
    @staticmethod
    def toInt(s: str) -> int:
        return InvertedBase62.getInstance().decodeToInt(s)


    ## Decodes a string to a double.
    # @param s Encoded string.
    # return Double. ##
    @staticmethod
    def toDouble(s: str) -> float:
        return InvertedBase62.getInstance().decodeToDouble(s)
