
import hashlib

from robbiblubber.util.coding._hash import _Hash
from robbiblubber.util.coding.base62 import Base62
from robbiblubber.util.coding.base64 import Base64
from robbiblubber.util.coding.hex import Hex
from robbiblubber.util.coding.iencoder import IEncoder
from robbiblubber.util.coding.invertedbase62 import InvertedBase62
from robbiblubber.util.coding.plaintext import PlainText
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class implements the SHA512 algorithm. ##
class SHA512(_Hash):
    ############################################################################
    # static members                                                           #
    ############################################################################

    ## Base64 instance. ##
    _base64 = None

    ## Base62 instance. ##
    _base62 = None

    ## InvertedBase62 instance. ##
    _invertedBase62 = None

    ## Hex instance. ##
    _hex = None

    ## PlainText instance. ##
    _plainText = None



    ############################################################################
    # constructors                                                             #
    ############################################################################

    ## Creates a new instance of this class. ##
    def __init__(self, encoder: IEncoder = None):
        super().__init__(encoder)



    ############################################################################
    # static methods                                                           #
    ############################################################################

    ## Gets an instance using Base64 encoding. ##
    @staticmethod
    def BASE64():
        if SHA512._base64 is None: SHA512._base64 = SHA512(Base64.getInstance())
        return SHA512._base64


    ## Gets an instance using Base62 encoding. ##
    @staticmethod
    def BASE62():
        if SHA512._base62 is None: SHA512._base62 = SHA512(Base62.getInstance())
        return SHA512._base62


    ## Gets an instance using inverted Base62 encoding. ##
    @staticmethod
    def INVERTED_BASE62():
        if SHA512._invertedBase62 is None: SHA512._invertedBase62 = SHA512(InvertedBase62.getInstance())
        return SHA512._invertedBase62


    ## Gets an instance using Base64 encoding. ##
    @staticmethod
    def HEX():
        if SHA512._hex is None: SHA512._hex = SHA512(Hex.getInstance())
        return SHA512._hex


    ## Gets an instance using Base64 encoding. ##
    @staticmethod
    def PLAINTEXT():
        if (SHA512._plainText is None): SHA512._plainText = SHA512(PlainText.getInstance())
        return SHA512._plainText



    ############################################################################
    # [override] __Hash                                                        #
    ############################################################################

    ## Hashes a byte array.
    # @param value Byte array.
    # @return Hash. ##
    def hash(self, value: bytearray) -> bytearray:
        return hashlib.sha512(value).digest()
