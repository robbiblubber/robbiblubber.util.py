
from robbiblubber.util.coding.iencoder import IEncoder
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## Classes that provide hash algorithms implement this interface. ##
class IHash:
    ############################################################################
    # properties                                                               #
    ############################################################################

    ## Gets the encoding.
    # @return Encoding. ##
    @property
    def encoder(self) -> IEncoder:
        pass



    ############################################################################
    # methods                                                                  #
    ############################################################################

    ## Hashes a byte array.
    # @param value Byte array.
    # @return Hash. ##
    def hashBytes(self, value: bytearray) -> str:
        pass


    ## Hashes a string.
    # @param value String.
    # @return Hash. ##
    def hashString(self, value: str) -> str:
        pass


    ## Hashes an integer.
    # @param value Integer.
    # @return Hash. ##
    def hashInt(self, value: int) -> str:
        pass


    ## Hashes a double.
    # @param value Double.
    # @return Hash. ##
    def hashDouble(self, value: float) -> str:
        pass


    ## Hashes a byte array.
    # @param value Byte array.
    # @return Hash. ##
    def hash(self, value: bytearray) -> bytearray:
        pass
