
from robbiblubber.util.coding._encoder import _Encoder
from robbiblubber.util.coding.base62 import Base62
from robbiblubber.util.coding.base64 import Base64
from robbiblubber.util.coding.basen import BaseN
from robbiblubber.util.coding.hex import Hex
from robbiblubber.util.coding.iencoder import IEncoder
from robbiblubber.util.coding.iencrypter import IEncrypter
from robbiblubber.util.coding.invertedbase62 import InvertedBase62
from robbiblubber.util.coding.plaintext import PlainText
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class implements the FunnelCake encryption algorithm. ##
from robbiblubber.util.coding.sha256 import SHA256


class FunnelCake(_Encoder, IEncrypter):
    ############################################################################
    # private constants                                                        #
    ############################################################################

    ## Seed string. ##
    _SEED = 'ZDQwZGI1YmFhNDVlNTM5ZTRmM2JiYjU4ZGQ1OGNjZDJkZDNiMjJiNThlYTBmYjE4MjdlOWNkNGFhM2U3Zjk5NQ'



    ############################################################################
    # static members                                                           #
    ############################################################################

    ## Base64 instance. ##
    _base64 = None

    ## Base62 instance. ##
    _base62 = None

    ## InvertedBase62 instance. ##
    _invertedBase62 = None

    ## Hex instance. ##
    _hex = None

    ## PlainText instance. ##
    _plainText = None



    ############################################################################
    # constructors                                                             #
    ############################################################################

    ## Creates a new instance of this class. ##
    def __init__(self, secret: str = None, encoder: IEncoder = None):
        if encoder is None:
            self._encoder = PlainText.getInstance()
        else:
            self._encoder = encoder

        if secret is None:
            self._secret = 'hUZ6wwgC20Qy'
        else:
            self._secret = secret

        super().__init__(self._encoder.encoding)



    ############################################################################
    # static methods                                                           #
    ############################################################################

    ## Gets an instance using Base64 encoding. ##
    @staticmethod
    def BASE64():
        if FunnelCake._base64 is None: FunnelCake._base64 = FunnelCake(encoder = Base64.getInstance())
        return FunnelCake._base64


    ## Gets an instance using Base62 encoding. ##
    @staticmethod
    def BASE62():
        if FunnelCake._base62 is None: FunnelCake._base62 = FunnelCake(encoder = Base62.getInstance())
        return FunnelCake._base62


    ## Gets an instance using inverted Base62 encoding. ##
    @staticmethod
    def INVERTED_BASE62():
        if FunnelCake._invertedBase62 is None: FunnelCake._invertedBase62 = FunnelCake(encoder = InvertedBase62.getInstance())
        return FunnelCake._invertedBase62


    ## Gets an instance using Base64 encoding. ##
    @staticmethod
    def HEX():
        if FunnelCake._hex is None: FunnelCake._hex = FunnelCake(encoder = Hex.getInstance())
        return FunnelCake._hex


    ## Gets an instance using Base64 encoding. ##
    @staticmethod
    def PLAINTEXT():
        if (FunnelCake._plainText is None): FunnelCake._plainText = FunnelCake(encoder = PlainText.getInstance())
        return FunnelCake._plainText


    ## Encrypts a byte array.
    # @param data Data.
    # @param secret Secret.
    # @return Encrypted data. ##
    @staticmethod
    def encrypt(data: bytearray, secret: str) -> bytearray:
        key = ''
        seed = FunnelCake._SEED
        sha256 = SHA256(BaseN(92))

        while(len(key) < len(data)):
            seed = sha256.hashString(secret + seed)
            key += seed

        k = bytearray(key, 'ascii')
        rval = bytearray()

        for i in range(len(data)):
            rval.append(data[i] ^ k[i])

        return rval


    ## Encrypts a string.
    # @param s String.
    # @param secret Secret.
    # @return Encrypted data. ##
    @staticmethod
    def encryptString(s: str, secret: str, encoder: IEncoder = None) -> str:
        if(encoder is None): encoder = PlainText.getInstance()
        return encoder.encodeBytes(FunnelCake.encrypt(bytearray(s, encoder.encoding), secret))


    ## Decrypts a byte array.
    # @param data Data.
    # @param secret Secret.
    # @return Decrypted data. ##
    @staticmethod
    def decrypt(data: bytearray, secret: str) -> bytearray:
        return FunnelCake.encrypt(data, secret)


    ## Decrypts a string.
    # @param s String.
    # @param secret Secret.
    # @return Decrypted data. ##
    @staticmethod
    def decryptString(s: str, secret: str, encoder: IEncoder = None) -> str:
        if (encoder is None): encoder = PlainText.getInstance()
        return FunnelCake.decrypt(encoder.decode(s)).decode(encoder.encoding)



    ############################################################################
    # [override] __Encoder                                                     #
    ############################################################################

    ## Encodes a byte array.
    # @param data Byte array.
    # @return Encoded byte array. ##
    def encode(self, data: bytearray) -> bytearray:
        return self._encoder.encode(FunnelCake.encrypt(data, self._secret))


    ## Decodes a byte array.
    # @param data Byte array.
    # @return Decoded byte array. ##
    def decode(self, data: bytearray) -> bytearray:
        return FunnelCake.decrypt(self._encoder.decode(data), self._secret)



    ############################################################################
    # [interface] IEncrypter                                                   #
    ############################################################################

    ## Gets the encoder. ##
    @property
    def encoder(self):
        return self._encoder


    ## Sets the secret. ##
    def setSecret(self, secret: str):
        self._secret = secret
