
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## Classes that provide encoding and decoding implement this interface. ##
class IEncoder:
    ############################################################################
    # properties                                                               #
    ############################################################################

    ## Gets the encoding.
    # @return Encoding. ##
    @property
    def encoding(self) -> str:
        pass



    ############################################################################
    # methods                                                                  #
    ############################################################################

    ## Converts bytes to an encoded string.
    # @param value Byte array.
    # @return Encoded string. ##
    def encodeBytes(self, value: bytearray) -> str:
        pass


    ## Converts a string to an encoded string.
    # @param value String.
    # @return Encoded string. ##
    def encodeString(self, value: str) -> str:
        pass


    ## Converts an integer to an encoded string.
    # @param value Integer.
    # @return Encoded string. ##
    def encodeInt(self, value: int) -> str:
        pass


    ## Converts a double to an encoded string.
    # @param value Double.
    # @return Encoded string. ##
    def encodeDouble(self, value: float) -> str:
        pass


    ## Converts an encoded string to a byte array.
    # @param s Encode string.
    # @return Byte array. ##
    def decodeToBytes(self, s: str) -> bytearray:
        pass


    ## Converts an encoded string to a string.
    # @param s Encode string.
    # @return String. ##
    def decodeToString(self, s: str) -> str:
        pass


    ## Converts an encoded string to an integer.
    # @param s Encode string.
    # @return Integer. ##
    def decodeToInt(self, s: str) -> int:
        pass


    ## Converts an encoded string to a double.
    # @param s Encode string.
    # @return Double. ##
    def decodeToDouble(self, s: str) -> float:
        pass


    ## Encodes a byte array.
    # @param data Byte array.
    # @return Encoded byte array. ##
    def encode(self, data: bytearray) -> bytearray:
        pass


    ## Decodes a byte array.
    # @param data Byte array.
    # @return Decoded byte array. ##
    def decode(self, data: bytearray) -> bytearray:
        pass
