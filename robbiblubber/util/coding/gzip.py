
import zlib

from robbiblubber.util.coding._encoder import _Encoder
from robbiblubber.util.coding.base62 import Base62
from robbiblubber.util.coding.base64 import Base64
from robbiblubber.util.coding.hex import Hex
from robbiblubber.util.coding.iencoder import IEncoder
from robbiblubber.util.coding.invertedbase62 import InvertedBase62
from robbiblubber.util.coding.plaintext import PlainText
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class provides GZIP support.
# @remarks Python implementation has compatibility issues with .NET, Java, and PHP version. ##
class GZip(_Encoder):
    ############################################################################
    # static members                                                           #
    ############################################################################

    ## Base64 instance. ##
    _base64 = None

    ## Base62 instance. ##
    _base62 = None

    ## InvertedBase62 instance. ##
    _invertedBase62 = None

    ## Hex instance. ##
    _hex = None

    ## PlainText instance. ##
    _plainText = None



    ############################################################################
    # constructors                                                             #
    ############################################################################

    ## Creates a new instance of this class. ##
    def __init__(self, encoder: IEncoder = None):
        if encoder is None:
            self._encoder = PlainText.getInstance()
        else:
            self._encoder = encoder

        super().__init__(self._encoder.encoding)



    ############################################################################
    # static methods                                                           #
    ############################################################################

    ## Gets an instance using Base64 encoding. ##
    @staticmethod
    def BASE64():
        if GZip._base64 is None: GZip._base64 = GZip(Base64.getInstance())
        return GZip._base64


    ## Gets an instance using Base62 encoding. ##
    @staticmethod
    def BASE62():
        if GZip._base62 is None: GZip._base62 = GZip(Base62.getInstance())
        return GZip._base62


    ## Gets an instance using inverted Base62 encoding. ##
    @staticmethod
    def INVERTED_BASE62():
        if GZip._invertedBase62 is None: GZip._invertedBase62 = GZip(InvertedBase62.getInstance())
        return GZip._invertedBase62


    ## Gets an instance using Base64 encoding. ##
    @staticmethod
    def HEX():
        if GZip._hex is None: GZip._hex = GZip(Hex.getInstance())
        return GZip._hex


    ## Gets an instance using Base64 encoding. ##
    @staticmethod
    def PLAINTEXT():
        if (GZip._plainText is None): GZip._plainText = GZip(PlainText.getInstance())
        return GZip._plainText


    ## Compresses a byte array.
    # @param data Byte array.
    # @return Compressed data. ##
    @staticmethod
    def compress(data: bytearray, encoder: IEncoder = None) -> bytearray:
        if encoder is None:
            return zlib.compress(data)

        return encoder.encode(zlib.compress(data))


    ## Decompresses a byte array.
    # @param data Byte array.
    # @return Decompressed data. ##
    @staticmethod
    def decompress(data: bytearray, encoder: IEncoder = None) -> bytearray:
        if encoder is None:
            return zlib.decompress(data)

        return zlib.decompress(encoder.decode(data))


    ## Compresses a string.
    # @param s String.
    # @return Compressed string. ##
    @staticmethod
    def compressString(s: str, encoder: IEncoder = None) -> str:
        if encoder is None: encoder = PlainText.getInstance()

        return GZip.compress(bytearray(s, encoder.encoding), encoder)


    ## Decompresses a String.
    # @param s String.
    # @return Decompressed string. ##
    @staticmethod
    def decompressString(s: str, encoder: IEncoder = None) -> bytearray:
        if encoder is None: encoder = PlainText.getInstance()

        return GZip.decompress(encoder.decodeToBytes(s)).decode(encoder.encoding)



    ############################################################################
    # properties                                                               #
    ############################################################################

    ## Gets the encoder. ##
    @property
    def encoder(self):
        return self._encoder



    ############################################################################
    # [override] __Encoder                                                     #
    ############################################################################

    ## Encodes a byte array.
    # @param data Byte array.
    # @return Encoded byte array. ##
    def encode(self, data: bytearray, pad: bool = False) -> bytearray:
        return GZip.compress(data, self._encoder)


    ## Decodes a byte array.
    # @param data Byte array.
    # @return Decoded byte array. ##
    def decode(self, data: bytearray) -> bytearray:
        return GZip.decompress(data, self._encoder)
