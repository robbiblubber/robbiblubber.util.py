
from robbiblubber.util.coding.iencoder import IEncoder
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class provides a plain text encoder. ##
class PlainText(IEncoder):
    ############################################################################
    # static members                                                           #
    ############################################################################

    ## Instance. ##
    _instance = None



    ############################################################################
    # constructor                                                              #
    ############################################################################

    ## Creates a new instance of this class.
    # @param encoding Encoding. ##
    def __init__(self, encoding: str = "utf-8"):
        super().__init__(encoding)


    ############################################################################
    # static methods                                                           #
    ############################################################################

    ## Gets the singleton instance.
    # @return Instance. ##
    @staticmethod
    def getInstance():
        if (PlainText._instance is None):
            PlainText._instance = PlainText()
        return PlainText._instance



    ############################################################################
    # [override] IEncoder                                                      #
    ############################################################################

    ## Converts bytes to an encoded string.
    # @param value Byte array.
    # @return Encoded string. ##
    def encodeBytes(self, value: bytearray) -> str:
        return str(value, self._encoding)


    ## Converts a string to an encoded string.
    # @param value String.
    # @return Encoded string. ##
    def encodeString(self, value: str) -> str:
        return value


    ## Converts an integer to an encoded string.
    # @param value Integer.
    # @return Encoded string. ##
    def encodeInt(self, value: int) -> str:
        return str(value)


    ## Converts a double to an encoded string.
    # @param value Double.
    # @return Encoded string. ##
    def encodeDouble(self, value: float) -> str:
        return str(value).replace(',', ',.')


    ## Converts an encoded string to a byte array.
    # @param s Encode string.
    # @return Byte array. ##
    def decodeToBytes(self, s: str) -> bytearray:
        return bytearray(s, self._encoding)


    ## Converts an encoded string to a string.
    # @param s Encode string.
    # @return String. ##
    def decodeToString(self, s: str) -> str:
        return s


    ## Converts an encoded string to an integer.
    # @param s Encode string.
    # @return Integer. ##
    def decodeToInt(self, s: str) -> int:
        return int(s)


    ## Converts an encoded string to a double.
    # @param s Encode string.
    # @return Double. ##
    def decodeToDouble(self, s: str) -> float:
        return float(s.replace(',', '.'))


    ## Encodes a byte array.
    # @param data Byte array.
    # @return Encoded byte array. ##
    def encode(self, data: bytearray) -> bytearray:
        return data

    ## Decodes a byte array.
    # @param data Byte array.
    # @return Decoded byte array. ##
    def decode(self, data: bytearray) -> bytearray:
        return data
