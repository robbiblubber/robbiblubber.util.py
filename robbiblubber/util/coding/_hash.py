
import struct

from robbiblubber.util.coding.hex import Hex
from robbiblubber.util.coding.iencoder import IEncoder
from robbiblubber.util.coding.ihash import IHash
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class provides a base implementation of the IHash interface. ##
class _Hash(IHash):
    ############################################################################
    # constructors                                                             #
    ############################################################################

    ## Creates a new instance of this class. ##
    def __init__(self, encoder: IEncoder = None):
        self._encoder = encoder



    ############################################################################
    # [interface] IHash                                                        #
    ############################################################################

    ## Gets the encoding.
    # @return Encoding. ##
    @property
    def encoder(self) -> IEncoder:
        if self._encoder is None:
            self._encoder = Hex.getInstance()

        return self._encoder


    ## Hashes a byte array.
    # @param value Byte array.
    # @return Hash. ##
    def hashBytes(self, value: bytearray) -> str:
        return self.encoder.encodeBytes(self.hash(value))


    ## Hashes a string.
    # @param value String.
    # @return Hash. ##
    def hashString(self, value: str) -> str:
        return self.encoder.encodeBytes(self.hash(bytearray(value, self.encoder.encoding)))


    ## Hashes an integer.
    # @param value Integer.
    # @return Hash. ##
    def hashInt(self, value: int) -> str:
        return self.encoder.encodeBytes(self.hash(bytearray(value.to_bytes(4, 'big'))))


    ## Hashes a double.
    # @param value Double.
    # @return Hash. ##
    def hashDouble(self, value: float) -> str:
        return self.encoder.encodeBytes(self.hash(bytearray(struct.pack('d', value))))


    ## Hashes a byte array.
    # @param value Byte array.
    # @return Hash. ##
    def hash(self, value: bytearray) -> bytearray:
        pass
