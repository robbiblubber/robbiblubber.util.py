
from robbiblubber.util.coding._encoder import _Encoder
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class provides hexadecimal encoding methods. ##
class Hex(_Encoder):
    ############################################################################
    # constants                                                                #
    ############################################################################

    ## The hexadecimal alphabet. ##
    ALPHABET: str = '0123456789abcdef'



    ############################################################################
    # static members                                                           #
    ############################################################################

    ## Instance. ##
    _instance = None



    ############################################################################
    # constructor                                                              #
    ############################################################################

    ## Creates a new instance of this class.
    # @param encoding Encoding. ##
    def __init__(self, encoding: str = "utf-8"):
        super().__init__(encoding)



    ############################################################################
    # static methods                                                           #
    ############################################################################

    ## Gets the singleton instance.
    # @return Instance. ##
    @staticmethod
    def getInstance():
        if (Hex._instance is None):
            Hex._instance = Hex()
        return Hex._instance


    ## Converts bytes to a hexadecimal string.
    # @param value Bytes.
    # return Encoded string. ##
    @staticmethod
    def fromBytes(value: bytearray) -> str:
        return Hex.getInstance().encodeBytes(value)


    ## Converts a string to a hexadecimal string.
    # @param value String.
    # return Encoded string. ##
    @staticmethod
    def fromString(value: str) -> str:
        return Hex.getInstance().encodeString(value)


    ## Converts an integer to a hexadecimal string.
    # @param value Integer.
    # return Encoded string. ##
    @staticmethod
    def fromInt(value: int) -> str:
        return Hex.getInstance().encodeInt(value)


    ## Converts a double to a hexadecimal string.
    # @param value Double.
    # return Encoded string. ##
    @staticmethod
    def fromDouble(value: float) -> str:
        return Hex.getInstance().encodeDouble(value)


    ## Decodes a string to a byte array.
    # @param s Encoded string.
    # return Byte array. ##
    @staticmethod
    def toBytes(s: str) -> bytearray:
        return Hex.getInstance().decodeToBytes(s)


    ## Decodes a string to a string.
    # @param s Encoded string.
    # return String. ##
    @staticmethod
    def toString(s: str) -> str:
        return Hex.getInstance().decodeToString(s)


    ## Decodes a string to an integer.
    # @param s Encoded string.
    # return Integer. ##
    @staticmethod
    def toInt(s: str) -> int:
        return Hex.getInstance().decodeToInt(s)


    ## Decodes a string to a double.
    # @param s Encoded string.
    # return Double. ##
    @staticmethod
    def toDouble(s: str) -> float:
        return Hex.getInstance().decodeToDouble(s)



    ############################################################################
    # methods                                                                  #
    ############################################################################

    ## Encodes a byte.
    # @param value Byte.
    # @param pad Add padding character.
    # @return String. ##
    def encodeChar(self, value: str, pad: bool = True) -> str:
        rval = hex(ord(value))[2:]
        return rval.zfill(2) if pad else rval


    ## Encodes a byte.
    # @param value Byte.
    # @param pad Add padding character.
    # @return String. ##
    def encodeByte(self, value: int, pad: bool = True) -> str:
        rval = hex(value)[2:]
        return rval.zfill(2) if pad else rval


    ## Decodes a byte.
    # @param value Byte.
    # @return Byte. ##
    def decodeToByte(self, value: str) -> int:
        return int(value, 16)


    ## Decodes a byte.
    # @param value Byte.
    # @return Byte. ##
    def decodeToChar(self, value: str) -> str:
        return chr(int(value, 16))



    ############################################################################
    # [override] __Encoder                                                     #
    ############################################################################

    ## Converts bytes to an encoded string.
    # @param value Byte array.
    # @return Encoded string. ##
    def encodeBytes(self, value: bytearray) -> str:
        return bytearray(value).hex()


    ## Converts an encoded string to a byte array.
    # @param s Encode string.
    # @return Byte array. ##
    def decodeToBytes(self, s: str) -> bytearray:
        return bytearray.fromhex(s)


    ## Encodes a byte array.
    # @param data Byte array.
    # @return Encoded byte array. ##
    def encode(self, data: bytearray) -> bytearray:
        return bytearray(self.encodeBytes((data)), self._encoding)


    ## Decodes a byte array.
    # @param data Byte array.
    # @return Decoded byte array. ##
    def decode(self, data: bytearray) -> bytearray:
        return self.decodeToBytes(data.decode(self._encoding))
