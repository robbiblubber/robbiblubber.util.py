
import base64

from robbiblubber.util.coding._encoder import _Encoder
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class provides Base64 support. ##
class Base64(_Encoder):
    ############################################################################
    # constants                                                                #
    ############################################################################

    ## The hexadecimal alphabet. ##
    ALPHABET: str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='



    ############################################################################
    # static members                                                           #
    ############################################################################

    ## Instance. ##
    _instance = None



    ############################################################################
    # constructor                                                              #
    ############################################################################

    ## Creates a new instance of this class.
    # @param encoding Encoding.
    # @param alphabet Alphabet. ##
    def __init__(self, encoding: str = "utf-8"):
        super().__init__(encoding)



    ############################################################################
    # static methods                                                           #
    ############################################################################

    ## Gets the singleton instance.
    # @return Instance. ##
    @staticmethod
    def getInstance():
        if Base64._instance is None:
            Base64._instance = Base64()
        return Base64._instance


    ## Converts bytes to a hexadecimal string.
    # @param value Bytes.
    # return Encoded string. ##
    @staticmethod
    def fromBytes(value: bytearray) -> str:
        return Base64.getInstance().encodeBytes(value)


    ## Converts a string to a hexadecimal string.
    # @param value String.
    # return Encoded string. ##
    @staticmethod
    def fromString(value: str) -> str:
        return Base64.getInstance().encodeString(value)


    ## Converts an integer to a hexadecimal string.
    # @param value Integer.
    # return Encoded string. ##
    @staticmethod
    def fromInt(value: int) -> str:
        return Base64.getInstance().encodeInt(value)


    ## Converts a double to a hexadecimal string.
    # @param value Double.
    # return Encoded string. ##
    @staticmethod
    def fromDouble(value: float) -> str:
        return Base64.getInstance().encodeDouble(value)


    ## Decodes a string to a byte array.
    # @param s Encoded string.
    # return Byte array. ##
    @staticmethod
    def toBytes(s: str) -> bytearray:
        return Base64.getInstance().decodeToBytes(s)


    ## Decodes a string to a string.
    # @param s Encoded string.
    # return String. ##
    @staticmethod
    def toString(s: str) -> str:
        return Base64.getInstance().decodeToString(s)


    ## Decodes a string to an integer.
    # @param s Encoded string.
    # return Integer. ##
    @staticmethod
    def toInt(s: str) -> int:
        return Base64.getInstance().decodeToInt(s)


    ## Decodes a string to a double.
    # @param s Encoded string.
    # return Double. ##
    @staticmethod
    def toDouble(s: str) -> float:
        return Base64.getInstance().decodeToDouble(s)

    ############################################################################
    # methods                                                                  #
    ############################################################################

    ## Reads a (binary) file into a Base64-encode string.
    # @param fileName File name.
    # @param pad Add padding characters.
    # @return Encoded string. ##
    def encodeFile(self, fileName: str, pad: bool = False) -> str:
        f = open(fileName, 'rb')
        rval = self.encodeBytes(f.read(), pad)
        f.close()

        return rval



    ############################################################################
    # [override] __Encoder                                                     #
    ############################################################################

    ## Converts bytes to an encoded string.
    # @param value Byta array.
    # @param pad Add padding characters.
    # @return Encoded string. ##
    def encodeBytes(self, value: bytearray, pad: bool = False) -> str:
        rval = base64.standard_b64encode(value).decode(self.encoding)
        if(pad):
            return rval
        else:
            return rval.strip('=')


    ## Converts a string to an encoded string.
    # @param value String.
    # @param pad Add padding characters.
    # @return Encoded string. ##
    def encodeString(self, value: str, pad: bool = False) -> str:
        return self.encodeBytes(bytearray(value, self._encoding), pad)


    ## Converts an encoded string to a byte array.
    # @param s Encode string.
    # @return Byte array. ##
    def decodeToBytes(self, s: str) -> bytearray:
        while len(s) % 4 != 0: s += '='
        return base64.standard_b64decode(bytearray(s, self._encoding))


    ## Encodes a byte array.
    # @param data Byte array.
    # @return Encoded byte array. ##
    def encode(self, data: bytearray, pad: bool = False) -> bytearray:
        return bytearray(self.encodeBytes(data, pad), self._encoding)


    ## Decodes a byte array.
    # @param data Byte array.
    # @return Decoded byte array. ##
    def decode(self, data: bytearray) -> bytearray:
        return self.decodeToBytes(data.decode(self._encoding))
