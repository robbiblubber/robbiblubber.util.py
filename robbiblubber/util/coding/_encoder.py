
import struct

from robbiblubber.util.coding.iencoder import IEncoder
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class provides a basic implementation of the IEncoder interface. ##
class _Encoder(IEncoder):
    ############################################################################
    # constructor                                                              #
    ############################################################################

    ## Creates a new instance of this class.
    # @param encoding Encoding. ##
    def __init__(self, encoding: str = "utf-8"):
        self._encoding = encoding



    ############################################################################
    # [interface] IEncoder                                                     #
    ############################################################################

    ## Gets the encoding.
    # @return Encoding. ##
    @property
    def encoding(self) -> str:
        return self._encoding


    ## Converts bytes to an encoded string.
    # @param value Byte array.
    # @return Encoded string. ##
    def encodeBytes(self, value: bytearray) -> str:
        return self.encode(bytearray(value)).decode(self._encoding)


    ## Converts a string to an encoded string.
    # @param value String.
    # @return Encoded string. ##
    def encodeString(self, value: str) -> str:
        return self.encodeBytes(bytearray(value, self._encoding))


    ## Converts an integer to an encoded string.
    # @param value Integer.
    # @return Encoded string. ##
    def encodeInt(self, value: int) -> str:
        return self.encodeBytes(bytearray(value.to_bytes(4, 'big')))


    ## Converts a double to an encoded string.
    # @param value Double.
    # @return Encoded string. ##
    def encodeDouble(self, value: float) -> str:
        return self.encodeBytes((bytearray(struct.pack('d', value))))


    ## Converts an encoded string to a byte array.
    # @param s Encode string.
    # @return Byte array. ##
    def decodeToBytes(self, s: str) -> bytearray:
        return self.decode(bytearray(s, self._encoding))


    ## Converts an encoded string to a string.
    # @param s Encode string.
    # @return String. ##
    def decodeToString(self, s: str) -> str:
        return self.decodeToBytes(s).decode(self._encoding)


    ## Converts an encoded string to an integer.
    # @param s Encode string.
    # @return Integer. ##
    def decodeToInt(self, s: str) -> int:
        return int.from_bytes(self.decodeToBytes(s), 'big')


    ## Converts an encoded string to a double.
    # @param s Encode string.
    # @return Double. ##
    def decodeToDouble(self, s: str) -> float:
        return struct.unpack('d', self.decodeToBytes(s))[0]


    ## Encodes a byte array.
    # @param data Byte array.
    # @return Encoded byte array. ##
    def encode(self, data: bytearray) -> bytearray:
        pass


    ## Decodes a byte array.
    # @param data Byte array.
    # @return Decoded byte array. ##
    def decode(self, data: bytearray) -> bytearray:
        pass
