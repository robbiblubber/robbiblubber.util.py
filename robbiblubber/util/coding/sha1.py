
import hashlib

from robbiblubber.util.coding._hash import _Hash
from robbiblubber.util.coding.base62 import Base62
from robbiblubber.util.coding.base64 import Base64
from robbiblubber.util.coding.hex import Hex
from robbiblubber.util.coding.iencoder import IEncoder
from robbiblubber.util.coding.invertedbase62 import InvertedBase62
from robbiblubber.util.coding.plaintext import PlainText
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class implements the SHA1 algorithm. ##
class SHA1(_Hash):
    ############################################################################
    # static members                                                           #
    ############################################################################

    ## Base64 instance. ##
    _base64 = None

    ## Base62 instance. ##
    _base62 = None

    ## InvertedBase62 instance. ##
    _invertedBase62 = None

    ## Hex instance. ##
    _hex = None

    ## PlainText instance. ##
    _plainText = None



    ############################################################################
    # constructors                                                             #
    ############################################################################

    ## Creates a new instance of this class. ##
    def __init__(self, encoder: IEncoder = None):
        super().__init__(encoder)



    ############################################################################
    # static methods                                                           #
    ############################################################################

    ## Gets an instance using Base64 encoding. ##
    @staticmethod
    def BASE64():
        if SHA1._base64 is None: SHA1._base64 = SHA1(Base64.getInstance())
        return SHA1._base64


    ## Gets an instance using Base62 encoding. ##
    @staticmethod
    def BASE62():
        if SHA1._base62 is None: SHA1._base62 = SHA1(Base62.getInstance())
        return SHA1._base62


    ## Gets an instance using inverted Base62 encoding. ##
    @staticmethod
    def INVERTED_BASE62():
        if SHA1._invertedBase62 is None: SHA1._invertedBase62 = SHA1(InvertedBase62.getInstance())
        return SHA1._invertedBase62


    ## Gets an instance using Base64 encoding. ##
    @staticmethod
    def HEX():
        if SHA1._hex is None: SHA1._hex = SHA1(Hex.getInstance())
        return SHA1._hex


    ## Gets an instance using Base64 encoding. ##
    @staticmethod
    def PLAINTEXT():
        if (SHA1._plainText is None): SHA1._plainText = SHA1(PlainText.getInstance())
        return SHA1._plainText



    ############################################################################
    # [override] __Hash                                                        #
    ############################################################################

    ## Hashes a byte array.
    # @param value Byte array.
    # @return Hash. ##
    def hash(self, value: bytearray) -> bytearray:
        return hashlib.sha1(value).digest()
