
from robbiblubber.util.coding._encoder import _Encoder
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class provides an generic implementation of a BaseN algorithm.
# @remarks Part of the code is based on the work of Daniel Destouche (https://github.com/ghost1face/base62)
#          published under MIT License (see https://github.com/ghost1face/base62/blob/master/LICENSE/). ##
class BaseN(_Encoder):
    ############################################################################
    # constructor                                                              #
    ############################################################################

    def __init__(self, base: int, alphabet: str = None, encoding: str = 'utf-8'):
        super(BaseN, self).__init__(encoding)

        self._base = base
        if alphabet is None:
            self._alphabet = " !#$%&()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_abcdefghijklmnopqrstuvwxyz{|}~"
        else:
            self._alphabet = alphabet



    ############################################################################
    # static methods                                                           #
    ############################################################################

    ## Encodes or decodes data.
    # @param data Data.
    # @param inBase Input base.
    # @param outBase Output base.
    # @return Converted byte array. ##
    @staticmethod
    def _convert(data: bytearray, inBase: int, outBase: int):
        rval = bytearray()

        while(len(data) > 0):
            count = len(data)
            q = bytearray()
            rem = 0

            for i in range(count):
                acc = data[i] + (rem * inBase)
                d = int((acc - (acc % outBase)) / outBase)
                rem = acc % outBase

                if (len(q) > 0) or (d != 0): q.append(d)

            rval.insert(0, rem)
            data = q

        return rval



    ############################################################################
    # properties                                                               #
    ############################################################################

    ## Gets the alphabet. ##
    @property
    def alphabet(self):
        return self._alphabet



    ############################################################################
    # methods                                                                  #
    ############################################################################

    ## Reads a (binary) file into a Base64-encode string.
    # @param fileName File name.
    # @param pad Add padding characters.
    # @return Encoded string. ##
    def encodeFile(self, fileName: str, pad: bool = False) -> str:
        f = open(fileName, 'rb')
        rval = self.encodeBytes(f.read(), pad)
        f.close()

        return rval



    ############################################################################
    # [override] __Encoder                                                     #
    ############################################################################

    ## Converts bytes to an encoded string.
    # @param value Byte array.
    # @return Encoded string. ##
    def encodeBytes(self, value: bytearray) -> str:
        rval = ''
        for i in BaseN._convert(value, 256, self._base):
            rval += self._alphabet[i]

        return rval


    ## Converts an encoded string to a byte array.
    # @param s Encode string.
    # @return Byte array. ##
    def decodeToBytes(self, s: str) -> bytearray:
        rval = bytearray()
        for i in range(len(s)):
            rval.append(self._alphabet.find(s[i]))

        return BaseN._convert(rval, self._base, 256)


    ## Encodes a byte array.
    # @param data Byte array.
    # @return Encoded byte array. ##
    def encode(self, data: bytearray) -> bytearray:
        return bytearray(self.encodeBytes(data), self._encoding)


    ## Decodes a byte array.
    # @param data Byte array.
    # @return Decoded byte array. ##
    def decode(self, data: bytearray) -> bytearray:
        return self.decodeToBytes(data.decode(self._encoding))
