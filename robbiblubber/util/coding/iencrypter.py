
from robbiblubber.util.coding.iencoder import IEncoder
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## Classes that provide encryption implement this interface. ##
class IEncrypter(IEncoder):
    ############################################################################
    # properties                                                               #
    ############################################################################

    ## Gets the encoder. ##
    @property
    def encoder(self):
        pass



    ############################################################################
    # methods                                                                  #
    ############################################################################

    ## Sets the secret. ##
    def setSecret(self, secret: str):
        pass
