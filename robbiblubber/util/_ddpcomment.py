
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class implements a ddp comment. ##
class _DdpComment:
    ############################################################################
    # constructor                                                              #
    ############################################################################

    def __init__(self, value: str = None):
        if value is None: value = ''
        self.value = value



    ############################################################################
    # properties                                                               #
    ############################################################################

    ## Gets if the comment is empty. ##
    @property
    def empty(self):
        return self.value.strip() == ''



    ############################################################################
    # properties                                                               #
    ############################################################################

    ## Resets the comment. ##
    def reset(self):
        self.value = ''
