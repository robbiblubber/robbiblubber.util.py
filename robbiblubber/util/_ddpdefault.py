
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class signals a ddp default value. ##
class _DdpDefault:
    ############################################################################
    # static members                                                           #
    ############################################################################

    ## Instance. ##
    _instance = None



    ############################################################################
    # constructor                                                              #
    ############################################################################

    ## Creates a new instance of this class. ##
    def __init__(self):
        self.isDefault = True



    ############################################################################
    # magic methods                                                            #
    ############################################################################

    ## Checks for instance equality.
    # @param other Other instance.
    # @return Returns TRUE if the instances are equal, otherwise returns FALSE. ##
    def __eq__(self, other):
        return isinstance(other, _DdpDefault)



    ############################################################################
    # static methods                                                           #
    ############################################################################

    ## Gets the default instance. ##
    @staticmethod
    def DEFAULT():
        if _DdpDefault._instance is None: _DdpDefault._instance = _DdpDefault()
        return _DdpDefault._instance
