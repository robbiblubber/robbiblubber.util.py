
from robbiblubber.util.collections.iimmutabledictionary import IImmutableDictionary
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## Extensible dictionaries implement this interface. ##
class IExtensibleDictionary(IImmutableDictionary):
    ############################################################################
    # methods                                                                  #
    ############################################################################
    ## Adds an item to the list.
    # @param key Key.
    # @param value Value. ##
    def add(self, key, value):
        pass


    ## Adds a range of items to the list.
    # @param values Values. ##
    def addRange(self, values: {}):
        pass
