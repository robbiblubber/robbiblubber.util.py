
from robbiblubber.util.collections.iimmutablelist import IImmutableList
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## Immutable dictionaries implement this interface. ##
class IImmutableDictionary(IImmutableList):
    ############################################################################
    # properties                                                               #
    ############################################################################

    ## Gets all keys for this dictionary. ##
    @property
    def keys(self):
        pass


    ## Gets all values for this dictionary. ##
    @property
    def values(self):
        pass


    ############################################################################
    # methods                                                                  #
    ############################################################################

    ## Gets the item value for an index.
    # @param index Index.
    # @return Value. ##
    def itemAt(self, index):
        pass


    ## Gets the item key for an index.
    # @param index Index.
    # @return Value. ##
    def keyAt(self, index):
        pass


    ## Returns the key for an object.
    # @param item Item.
    # @return Key. ##
    def keyOf(self, item):
        pass


    ## Returns if the dictionary contains a given key.
    # @param item Item.
    # @return Index. ##
    def containsKey(self, key) -> bool:
        pass


    ## Gets the index of an item key.
    # @param key Key.
    # @return Index. ##
    def indexOfKey(self, key) -> int:
        pass


    ## Returns a dictionary copy of this collection.
    # @return Dictionary. ##
    def toDictionary(self) -> {}:
        pass
