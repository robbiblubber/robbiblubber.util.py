
from robbiblubber.util.collections.extensiblelist import ExtensibleList
from robbiblubber.util.collections.imutablelist import IMutableList
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## Mutable collections implement this interface. ##
class MutableList(ExtensibleList, IMutableList):
    ############################################################################
    # [interface] IMutableList                                                 #
    ############################################################################

    ## Gets an item by its index.
    # @param index Index.
    # @return Item. ##
    def __setitem__(self, index: int, value):
        self._items[index] = value


    ## Deletes an item.
    # @param item Item. #
    def __delitem__(self, item):
        self.removeAt(item)


    ## Removes the first occurence of an item from the list.
    # @param item Item. ##
    def remove(self, item):
        self._items.remove(item)


    ## Removes the item with the given index from the list.
    # @param index Index. ##
    def removeAt(self, index: int):
        self._items.pop(index)


    ## Removes all items from the list. ##
    def clear(self):
        self._items.clear()
