
from robbiblubber.util.collections.extensibledictionary import ExtensibleDictionary
from robbiblubber.util.collections.imutabledictionary import IMutableDictionary
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class provides an IMutableDictionary container for collections. ##
class MutableDictionary(ExtensibleDictionary, IMutableDictionary):
    ############################################################################
    # methods                                                                  #
    ############################################################################

    ## Deletes an item.
    # @param item Item. #
    def __delitem__(self, item):
        self.remove(item)


    ## Removes the item with a given key.
    # @param key Key. ##
    def remove(self, key):
        self._items.pop(key);


    ## Removes the item at a given index.
    # @param index Index. ##
    def removeAt(self, index):
        self._items.pop(self.keyAt(index))
