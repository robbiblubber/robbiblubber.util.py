
from robbiblubber.util.collections.iextensiblelist import IExtensibleList
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## Mutable collections implement this interface. ##
class IMutableList(IExtensibleList):
    ############################################################################
    # magic methods                                                            #
    ############################################################################

    ## Gets an item by its index.
    # @param index Index.
    # @return Item. ##
    def __setitem__(self, index: int, value):
        pass

    ## Deletes an item.
    # @param item Item. #
    def __delitem__(self, item):
        pass



    ############################################################################
    # methods                                                                  #
    ############################################################################

    ## Removes the first occurence of an item from the list.
    # @param item Item. ##
    def remove(self, item):
        pass


    ## Removes the item with the given index from the list.
    # @param index Index. ##
    def removeAt(self, index: int):
        pass


    ## Removes all items from the list. ##
    def clear(self):
        pass
