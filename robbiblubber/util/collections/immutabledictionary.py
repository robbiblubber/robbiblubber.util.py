
from robbiblubber.util.collections.iimmutabledictionary import IImmutableDictionary
from robbiblubber.util.collections.immutablelist import ImmutableList
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class provides an IImmutableDictionary container for collections. ##
class ImmutableDictionary(ImmutableList, IImmutableDictionary):
    ############################################################################
    # constructors                                                             #
    ############################################################################

    ## Creates a new instance of this class.
    # @param items
    def __init__(self, items: {} = None):
        self._items = {}

        if items is not None:
            for i in items.keys(): self._items[i] = items[i]



    ############################################################################
    # [override] ImmutableList                                                 #
    ############################################################################

    ## Gets an item by its index.
    # @param index Index.
    # @return Item. ##
    def __getitem__(self, key):
        return self._items[key]


    ## Gets an iterator for this class.
    # @return Iterator. ##
    def __iter__(self):
        return self._items.values().__iter__()


    ## Returns if the list contains an item.
    # @param item Item.
    # @return Returns TRUE if the collection contains the item, otherwise returns FALSE. ##
    def contains(self, item) -> bool:
        return item in self._items.values()


    ## Gets the index of an item.
    # @param item Item.
    # @return Index. ##
    def indexOf(self, item) -> int:
        return list(self._items.values()).index(item)


    ## Returns a list copy of this collection.
    # @return List. ##
    def toList(self) -> []:
        return list(self._items.values())



    ############################################################################
    # [interface] IImmutableDictionary                                         #
    ############################################################################

    ## Gets all keys for this dictionary. ##
    @property
    def keys(self):
        return list(self._items.keys())


    ## Gets all values for this dictionary. ##
    @property
    def values(self):
        return list(self._items.values())


    ## Gets the item value for an index.
    # @param index Index.
    # @return Value. ##
    def itemAt(self, index):
        return list(self._items.values())[index]


    ## Gets the item key for an index.
    # @param index Index.
    # @return Value. ##
    def keyAt(self, index):
        return list(self._items.keys())[index]


    ## Returns the key for an object.
    # @param item Item.
    # @return Key. ##
    def keyOf(self, item):
        return list(self._items.keys())[list(self._items.values()).index(item)]


    ## Returns if the dictionary contains a given key.
    # @param item Item.
    # @return Index. ##
    def containsKey(self, key) -> bool:
        return key in self._items.keys()


    ## Gets the index of an item key.
    # @param key Key.
    # @return Index. ##
    def indexOfKey(self, item) -> int:
        return list(self._items.keys()).index(item)


    ## Returns a dictionary copy of this collection.
    # @return Dictionary. ##
    def toDictionary(self) -> {}:
        return self._items.copy()
