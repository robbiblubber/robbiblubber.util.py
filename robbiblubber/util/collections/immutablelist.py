
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class provides an IImmutableList container for collections. ##
class ImmutableList:
    ############################################################################
    # constructors                                                             #
    ############################################################################

    ## Creates a new instance of this class.
    # @param items
    def __init__(self, items: [] = None):
        self._items = []

        if items is not None:
            for i in items: self._items.append(i)



    ############################################################################
    # [interface] IImmutableList                                               #
    ############################################################################

    ## Gets an item by its index.
    # @param index Index.
    # @return Item. ##
    def __getitem__(self, index: int):
        return self._items[index]


    ## Gets the number of items in the list.
    # @return Item count. ##
    def __len__(self):
        return self.count


    ## Gets an iterator for this class.
    # @return Iterator. ##
    def __iter__(self):
        return self._items.__iter__()


    ## Gets a string representaion of this instance.
    # @return String. #
    def __str__(self):
        return str(self._items)


    ## Gets the number of items in this list. ##
    @property
    def count(self):
        return len(self._items)


    ## Returns if the list contains an item.
    # @param item Item.
    # @return Returns TRUE if the collection contains the item, otherwise returns FALSE. ##
    def contains(self, item) -> bool:
        return item in self._items


    ## Gets the index of an item.
    # @param item Item.
    # @return Index. ##
    def indexOf(self, item) -> int:
        return self._items.index(item)


    ## Returns a list copy of this collection.
    # @return List. ##
    def toList(self) -> []:
        return self._items.copy()
