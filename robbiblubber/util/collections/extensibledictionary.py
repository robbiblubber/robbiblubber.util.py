
from robbiblubber.util.collections.iextensibledictionary import IExtensibleDictionary
from robbiblubber.util.collections.immutabledictionary import ImmutableDictionary
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## Extensible dictionaries implement this interface. ##
class ExtensibleDictionary(ImmutableDictionary, IExtensibleDictionary):
    ############################################################################
    # [interface] IExtensibleDictionary                                        #
    ############################################################################
    ## Adds an item to the list.
    # @param key Key.
    # @param value Value. ##
    def add(self, key, value):
        if(key in self._items.keys()):
            raise KeyError

        self._items[key] = value


    ## Adds a range of items to the list.
    # @param values Values. ##
    def addRange(self, values: {}):
        for i in values.keys(): self._items[i] = values[i]
