
from robbiblubber.util.collections.iextensiblelist import IExtensibleList
from robbiblubber.util.collections.immutablelist import ImmutableList
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class provides an IExtensibleList container for collections. ##
class ExtensibleList(ImmutableList, IExtensibleList):
    ############################################################################
    # [interface] IExtesibleList                                               #
    ############################################################################

    ## Adds an item to the list.
    # @param item Item. ##
    def add(self, item):
        self._items.append(item);


    ## Adds a range of items to the list.
    # @param items Items. ##
    def addRange(self, items: []):
        for i in items: self._items.append(i)


    ## Inserts an item at a given position.
    # @param i Index.
    # @param item Item. ##
    def insert(self, i: int, item):
        self.insert(i, item)
