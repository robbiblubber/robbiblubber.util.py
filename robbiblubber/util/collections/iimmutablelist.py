
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## Immutable lists implement this interface. ##
class IImmutableList:
    ############################################################################
    # magic methods                                                            #
    ############################################################################

    ## Gets an item by its index.
    # @param index Index.
    # @return Item. ##
    def __getitem__(self, index: int):
        pass


    ## Gets the number of items in the list.
    # @return Item count. ##
    def __len__(self):
        pass


    ## Gets an iterator for this list.
    # @return Iterator. ##
    def __iter__(self):
        pass


    ## Gets a string representaion of this instance.
    # @return String. #
    def __str__(self):
        pass



    ############################################################################
    # properties                                                               #
    ############################################################################

    ## Gets the number of items in this list. ##
    @property
    def count(self):
        pass



    ############################################################################
    # methods                                                                  #
    ############################################################################

    ## Returns if the list contains an item.
    # @param item Item.
    # @return Returns TRUE if the collection contains the item, otherwise returns FALSE. ##
    def contains(self, item) -> bool:
        pass


    ## Gets the index of an item.
    # @param item Item.
    # @return Index. ##
    def indexOf(self, item) -> int:
        pass


    ## Returns a list copy of this collection.
    # @return List. ##
    def toList(self) -> []:
        pass
