
from robbiblubber.util.collections.iimmutablelist import IImmutableList
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## Extensible lists implement this interface. ##
class IExtensibleList(IImmutableList):
    ############################################################################
    # methods                                                                  #
    ############################################################################

    ## Adds an item to the list.
    # @param item Item. ##
    def add(self, item):
        pass

    ## Adds a range of items to the list.
    # @param items Items. ##
    def addRange(self, items: []):
        pass


    ## Inserts an item at a given position.
    # @param i Index.
    # @param item Item. ##
    def insert(self, i: int, item):
        pass
