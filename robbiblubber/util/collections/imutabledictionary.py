
from robbiblubber.util.collections.iextensibledictionary import IExtensibleDictionary
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## Mutable dictionaries implement this interface. ##
class IMutableDictionary(IExtensibleDictionary):
    ############################################################################
    # magic methods                                                            #
    ############################################################################

    ## Deletes an item.
    # @param item Item. #
    def __delitem__(self, item):
        pass


    ############################################################################
    # methods                                                                  #
    ############################################################################

    ## Removes the item with a given key.
    # @param key Key. ##
    def remove(self, key):
        pass

    ## Removes the item at a given index.
    # @param index Index. ##
    def removeAt(self, index):
        pass
