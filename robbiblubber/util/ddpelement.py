
from datetime import datetime, date

from robbiblubber.util._ddpcomment import _DdpComment
from robbiblubber.util._ddpdefault import _DdpDefault
from robbiblubber.util._ddpline import _DdpLine
from robbiblubber.util._ddputility import _DdpUtility
from robbiblubber.util.calendarop import CalendarOp
from robbiblubber.util.coding.iencoder import IEncoder
from robbiblubber.util.collectionop import CollectionOp
from robbiblubber.util.stringbuilder import StringBuilder
from robbiblubber.util.stringrdr import StringRdr
from robbiblubber.util.stringop import StringOp
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class implements a ddp element. ##
class DdpElement:
    ############################################################################
    # constructor                                                              #
    ############################################################################

    ## Creates a new instance of this class.
    # @param parent Parent element.
    # @param maskedName Masked name.
    # @param re String reader.
    # @param maskedValue Masked value.
    # @param value Value. ##
    def __init__(self, parent, maskedName: str, re: StringRdr = None, maskedValue: str = '', value: str = ''):
        self._parent = parent
        self._maskedName = maskedName
        self._maskedValue = maskedValue
        self._value = value
        self._comment = ''

        self._childElements = {}

        if re is not None:
            self._read(maskedName, re)



    ############################################################################
    # magic methods                                                            #
    ############################################################################

    ## Gets the child element or value for a path.
    # @param path Path.
    # @param t Type.
    # @param defaultValue Default value.
    # @return Element. ##
    def __getitem__(self, path: str, t: type = None, defaultValue = _DdpDefault.DEFAULT()):
        if(isinstance(path, tuple)):
            if len(path) == 3:
                path, t, defaultValue = path
            else:
                path, t = path
                defaultValue = _DdpDefault.DEFAULT()

                if type(t) == str:
                    t, defaultValue = str, t
                elif type(t) == int:
                    t, defaultValue = int, t
                elif type(t) == float:
                    t, defaultValue = float, t
                if type(t) == bool:
                    t, defaultValue = bool, t
                if type(t) == datetime:
                    t, defaultValue = datetime, t

                return self.get(path, t, defaultValue)
        else:
            return self._getChildElement(path)


    ## Sets the child item value for a path.
    # @param path Path.
    # @param value Value. ##
    def __setitem__(self, path, value):
        self._getChildElement(path).setValue(value)


    ## Delets a child item.
    # @param path Path. #
    def __delitem__(self, path):
        self._getChildElement(path).remove()


    ## Returns an integer value for this instance.
    # @return Integer. ##
    def __int__(self):
        return self.getValue(int)


    ## Returns an integer value for this instance.
    # @return Integer. ##
    def __float__(self):
        return self.getValue(float)


    ## Returns a string value for this instance.
    # @return String. ##
    def __str__(self):
        return self.getValue(str)


    ## Returns a Boolean value for this instance.
    # @return Boolean. ##
    def __bool__(self):
        return self.getValue(bool)


    ## Returns the number of elements in this instance.
    # @return Element count. ##
    def __len__(self):
        return self.elementCount



    ############################################################################
    # properties                                                               #
    ############################################################################

    ## Gets or sets the value. ##
    @property
    def value(self):
        return self._value


    ## Gets or sets the value. ##
    @value.setter
    def value(self, value):
        self.setValue(value)


    ## Gets the element name. ##
    @property
    def name(self):
        return _DdpUtility.unmaskSpecialChars(self._maskedName)


    ## Gets or sets the comment for this element. ##
    @property
    def comment(self) -> str:
        return self._comment


    ## Gets or sets the comment for this element. ##
    @comment.setter
    def comment(self, value: str):
        self._comment = value


    ## Gets or sets the element text. ##
    @property
    def text(self) -> str:
        return self.toString()


    ## Gets or sets the element text. ##
    @text.setter
    def text(self, value: str):
        re = StringRdr(value)
        self._read('', re)


    ## Gets if the element has child elements. ##
    @property
    def hasElements(self):
        return (len(self._childElements) > 0)


    ## Gets the number of child elements for this element. ##
    @property
    def elementCount(self):
        return len(self._childElements)


    ## Gets the section child elements of this element. ##
    @property
    def sections(self):
        rval = []
        for i in self._childElements:
            if i.hasElements: rval.append(i)
        return rval


    ## Gets the plain value child elements of this element. ##
    @property
    def values(self):
        rval = []
        for i in self._childElements:
            if not i.hasElements: rval.append(i)
        return rval


    ## Gets the parent element. ##
    @property
    def parent(self):
        return self._parent


    ## Gets the root element. ##
    @property
    def root(self):
        rval = self
        while rval._parent is not None:
            rval = rval._parent

        return rval



    ############################################################################
    # methods                                                                  #
    ############################################################################

    ## Gets a child element by its path.
    # @param path Path.
    # @return Child element. ##
    def _getChildElement(self, path):
        if type(path) == str: path = _DdpUtility.parsePath(path)
        if len(path) == 0: return self

        next = path[0]
        path.pop(0)

        if next == '': return self.root._getChildElement(path)
        if next == '.': return self._getChildElement(path)
        if next == '..':
            if self._parent is None: return self._getChildElement(path)
            return self._parent._getChildElement(path)

        if next not in self._childElements:
            self._childElements[next] = DdpElement(self, next)
        return self._childElements[next]._getChildElement(path)


    ## Creates a string representation of this instance.
    # @param s String builder.
    # @param indent Indent.
    # @param singleLine Creates the output as a single line.
    # @param skipComments Creates the output without comments.
    def _toString(self, s: StringBuilder, indent: int, singleLine: bool, skipComments: bool):
        hasName = not StringOp.isNullOrWhitespace(self._maskedName)
        ind = '' if singleLine else ('    ' * indent)

        if not (skipComments or StringOp.isNullOrWhitespace(self._comment)):
            s.append(ind)
            s.append('/* ')
            s.append(self._comment)
            s.append(' */')
            s.append(' ' if singleLine else '\r\n')

        if len(self._childElements) == 0:
            s.append(ind)
            s.append(_DdpUtility.internalToMask(self._maskedName) if hasName else '.')
            s.append(' = ')
            s.append(_DdpUtility.internalToMask(self._maskedValue))
            s.append(';')
            s.append(' ' if singleLine else '\r\n')
        else:
            if hasName:
                s.append(ind)
                s.append(_DdpUtility.internalToMask(self._maskedName))
                s.append(' { ' if singleLine else ('\r\n' + ind + '{\r\n'))

            hasEntries = False
            if not StringOp.isNullOrWhitespace(self._value):
                hasEntries = True
                s.append(ind)
                if (hasName and (not singleLine)): s.append('    ')
                s.append('. = ')
                s.append(_DdpUtility.internalToMask(self._maskedValue))
                s.append('; ' if singleLine else ';\r\n')

            for i in self._childElements.values():
                if i.hasElements: continue
                hasEntries = True
                i._toString(s, (indent + 1) if hasName else indent, singleLine, skipComments)

            for i in self._childElements.values():
                if not i.hasElements: continue
                if hasEntries:
                    hasEntries = False
                    if not singleLine: s.append(ind + '\r\n')

                i._toString(s, (indent + 1) if hasName else indent, singleLine, skipComments)

            if hasName:
                s.append(ind)
                s.append(' } ' if singleLine else '}\r\n')


    ## Reads a section.
    # @param maskedName Masked name.
    # @param re String reader. ##
    def _read(self, maskedName: str, re: StringRdr):
        comment = _DdpComment()
        v = None

        debug = re.string

        while True:
            line = _DdpUtility.readLine(re, comment)

            if line.termination == _DdpLine.BRACKET_OPEN:
                v = DdpElement(self, _DdpUtility.applyInternal(line.text.strip()), re)
                self._childElements[v._maskedName] = v
            else:
                if not StringOp.isNullOrWhitespace(line.text):
                    eqpos = line.text.index('=')
                    v = DdpElement(self, maskedName = _DdpUtility.applyInternal(line.text[:eqpos].strip(), True),
                                         maskedValue = _DdpUtility.applyInternal(line.text[eqpos + 1:].strip(), True),
                                         value = _DdpUtility.unmaskSpecialChars(line.text[eqpos + 1:].strip()))
                    if v._maskedName == '.':
                        self._maskedValue = v._maskedValue
                        self._value = v._value
                    else:
                        self._childElements[v._maskedName] = v
                if not (line.termination == _DdpLine.SEMICOLON): break

            if not comment.empty:
                v.comment = comment.value
                comment.reset()


    ## Gets the element value as a given type.
    # @param t Type.
    # @param defaultValue Default value.
    # @return Value. ##
    def getValue(self, t: type = str, defaultValue = _DdpDefault.DEFAULT()):
        if not isinstance(defaultValue, _DdpDefault) and ((self._value is None) or (self._value == '')):
            return defaultValue

        t = _DdpUtility.parseType(t)
        if t == _DdpUtility.STR:
            return self._value
        if t == _DdpUtility.INT:
            try:
                return int(self._value)
            except:
                return _DdpUtility.getDefault(defaultValue, 0)
        if t == _DdpUtility.FLOAT:
            try:
                return float(self._value)
            except:
                return _DdpUtility.getDefault(defaultValue, float(0))
        if t == _DdpUtility.DATETIME:
            if defaultValue == _DdpDefault.DEFAULT():
                defaultValue = datetime(1970, 1, 1)
            return CalendarOp.parseTimestamp(self._value, defaultValue)

        rval = self._maskedValue.split(',')

        if t == _DdpUtility.INT_ARRAY:
            try:
                for i in range(len(rval)): rval[i] = int(rval[i].strip())
            except:
                rval[i] = 0
        elif t == _DdpUtility.FLOAT_ARRAY:
            try:
                for i in range(len(rval)): rval[i] = float(rval[i].strip())
            except:
                rval[i] = float(0)
        elif t == _DdpUtility.DATETIME_ARRAY:
            for i in range(len(rval)): rval[i] = CalendarOp.parseTimestamp(rval[i].strip())
        else:
            for i in range(len(rval)): rval[i] = _DdpUtility.unmaskSpecialChars(rval[i].strip())

        return rval


    ## Sets the value.
    # @param value Value. ##
    def setValue(self, value):
        if type(value) == str:
            self._maskedValue = _DdpUtility.applyInternal(value)
            self._value = value
        elif (type(value) == datetime) or (type(value) == date):
            self._maskedValue = self.value = CalendarOp.toTimestamp(value)
        elif CollectionOp.isIterable(value):
            for i in range(len(value)):
                if i > 0:
                    self._maskedValue += ', '
                    self._value += ', '
                if type(value[i]) == str:
                    self._maskedValue += _DdpUtility.applyInternal(value[i])
                    self._value += value[i]
                else:
                    self._maskedValue += str(value[i])
                    self._value += str(value[i])
        else:
            self._maskedValue = self.value = str(value)


    ## Gets the element value for a path as a given type.
    # @param path Path.
    # @param t Type.
    # @param defaultValue Default value.
    # @return Value. ##
    def get(self, path: str, t: type, defaultValue = _DdpDefault.DEFAULT()):
        return self._getChildElement(path).getValue(t, defaultValue)


    ## Sets the element value for a path.
    # @param path Path.
    # @param value Value. ##
    def set(self, path: str, value):
        self._getChildElement(path).setValue(value)


    ## Removes this element. ##
    def remove(self):
        if self._parent is not None:
            del self._parent._childElements[self._maskedName]


    ## Removes all values and child elements from this element. ##
    def clear(self):
        self._maskedName = self._value = ''
        self._childElements = {}


    ## Moves this element to another element.
    # @param target Target element. ##
    def moveTo(self, target):
        self.remove()
        if self._maskedName in target._childElements:
            target = target._childElements[self._maskedName]
            target._maskedValue = self._maskedValue
            target._value = self._value

            for i in self._childElements:
                i.moveTo(target)
        else:
            self.parent = target
            target._childElements[self._maskedName] = self


    ## Moves this element to another element.
    # @param target Target element. ##
    def copyTo(self, target):
        if self._maskedName in target._childElements:
            target = target._childElements[self._maskedName]
            target._maskedValue = self._maskedValue
            target._value = self._value
        else:
            target = target._childElements[self._maskedName] = DdpElement(self._maskedName, maskedValue = self._maskedValue, value = self._value)

        for i in self._childElements:
            i.copyTo(target)


    ## Returns a string representation of this instance.
    # @param encoder Encoder.
    # @param singleLine Creates the output as a single line.
    # @param Creates the output without comments.
    # @return String. ##
    def toString(self, encoder: IEncoder = None, singleLine: bool = False, skipComments: bool = False):
        rval = StringBuilder()
        self._toString(rval, 0, singleLine, skipComments)

        if encoder is None:
            return rval.toString().strip() if singleLine else rval.toString()

        return encoder.encodeString(rval.toString().strip() if singleLine else rval.toString())


    ## Saves the element to a file.
    # @param file File name.
    # @param encoder Encoder. ##
    def save(self, file: str, encoder: IEncoder = None):
        with open(file, 'w') as f:
            f.write(self.toString(encoder))
