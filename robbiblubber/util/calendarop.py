
from datetime import datetime, timezone, timedelta, tzinfo

import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class provides Date/Time extension methods. ##
class CalendarOp:
    ############################################################################
    # static methods                                                           #
    ############################################################################

    ## Parses a date/time value to a timestamp string.
    # @param value Value.
    # @return Timestamp string. ##
    @staticmethod
    def toTimestamp(value: datetime) -> str:
        off = ' +00:00'
        try:
            if value.utcoffset() is not None:
                off = value.strftime(' %z')[0:4] + ':' + value.strftime('%z')[3:5]
        except: pass

        return value.strftime('%Y-%m-%d %H:%M:%S.') + value.strftime('%f')[0:3] + off



    ## Parses a timestamp string to date/time value.
    # @param value Value.
    # @param defaultValue Default value.
    # @param utc Determines if the DateTime object should be UTC.
    # @return Date/time value. ##
    @staticmethod
    def parseTimestamp(value: str, defaultValue: datetime = None, utc: bool = False) -> datetime:
        year = int(value[0:4])
        month = int(value[5:7])
        day = int(value[8:10])
        hour = min = sec = ms = 0
        off = None

        if len(value) > 15:
            hour = int(value[11:13])
            min = int(value[14:16])

        if len(value) > 18:
            sec = int(value[17:19])

        if len(value) > 20:
            startOff = 20
            if value[19] == '.':
                startOff = 24
                ms = int(value[20:23])

            ofs = (int(value[startOff:startOff + 3]) * 3600)
            if ofs < 0:
                ofs -= (int(value[startOff + 4:startOff + 6]) * 60)
            else:
                ofs += (int(value[startOff + 4:startOff + 6]) * 60)

            off = timezone(timedelta(seconds=ofs))

        return datetime(year=year, month=month, day=day, hour=hour, minute=min, second=sec, microsecond=ms * 1000, tzinfo=off)


    ## Converts a Unix timestamp to a DateTime value.
    # @param u Unix timestamp.
    # @return Date/time value. ##
    @staticmethod
    def fromUnixTimestamp(u: int) -> datetime:
        return datetime.utcfromtimestamp(u)


    ## Converts a DateTime value to a Unix timestamp.
    # @param d Date/time.
    # @return Unix timestamp. ##
    @staticmethod
    def toUnixTimestamp(d: datetime) -> int:
        return int(d.timestamp())


    ## Converts a Unix timestamp to a DateTime value.
    # @param u Unix timestamp.
    # @return Date/time value. ##
    @staticmethod
    def fromUnixTimestampMilliseconds(u: int) -> datetime:
        return datetime.utcfromtimestamp(u / 1000)


    ## Converts a DateTime value to a Unix timestamp.
    # @param d Date/time.
    # @return Unix timestamp. ##
    @staticmethod
    def toUnixTimestampMilliseconds(d: datetime) -> int:
        return int(d.timestamp() * 1000)
