
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class provides mathematical methods. ##
class MathOp:
    ############################################################################
    # static methods                                                           #
    ############################################################################

    ## Returns a value raised to a specific power.
    # @param n Number value.
    # @param power Power.
    # @return Result. ##
    @staticmethod
    def power(n, power):
        if (power == 0): return 1

        rval = n
        for i in range(1, power):
            rval *= n

        return rval


    ## Returns the greatest common factor for two integers.
    # @param a Number value.
    # @param b Another number value.
    # @return Returns the GCF for the given integers. ##
    @staticmethod
    def gcf(a: int, b: int) -> int:
        while(b != 0):
            t = b
            b = a % b
            a = t
        return a


    ## Returns the least common multiple for two integers.
    # @param a Number value.
    # @param b Another number value.
    # @return Returns the LCM for the given integers. ##
    @staticmethod
    def lcm(a: int, b: int) -> int:
        return int((a / MathOp.gcf(a, b)) * b)
