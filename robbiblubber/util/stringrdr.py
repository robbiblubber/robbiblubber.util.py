
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class implements a string reader. ##
class StringRdr:
    ############################################################################
    # constructor                                                              #
    ############################################################################

    ## Creates a new instance of this class.
    # @param s String.
    # @param position Position. ##
    def __init__(self, s: str = None, position: int = 0):
        self._string = s
        self._position = position
        self._length = 0

        if(s is not None): self._length = len(s)



    ############################################################################
    # megic methods                                                            #
    ############################################################################

    ## Returns a string representation of this instance. ##
    def __str__(self):
        return self._string



    ############################################################################
    # properties                                                               #
    ############################################################################

    ## Gets or sets the underlying string. ##
    @property
    def string(self) -> str:
        return self._string


    ## Gets or sets the underlying string. ##
    @string.setter
    def string(self, value: str):
        self._string = value
        self._length = 0 if value is None else len(value)


    ## Gets or sets the read position. ##
    @property
    def position(self) -> int:
        return self._position


    ## Gets or sets the read position. ##
    @position.setter
    def position(self, value: int):
        self._position = value


    ## Gets the string length. ##
    @property
    def length(self) -> int:
        return self._length


    ## Gets the current character. ##
    @property
    def current(self) -> str:
        return self._string[self._position - 1]


    ## Gets if more characters can be read from the current position. ##
    @property
    def more(self) -> bool:
        return (self._length > self._position)


    ## Gets the preceding character. ##
    @property
    def preceding(self) -> str:
        if self._position < 2: return chr(0)
        return self._string[self._position - 2]


    ## Gets the next character. ##
    @property
    def next(self):
        if self._position >= self._length: return chr(0)
        return self._string[self._position]


    ## Gets the part of the string from the current position. ##
    @property
    def tail(self):
        return self._string[self._position:]


    ## Gets the part of the string from the beginning to the current position. ##
    @property
    def lead(self):
        return self._string[:self._position]



    ############################################################################
    # properties                                                               #
    ############################################################################

    ## Resets the reader. ##
    def reset(self):
        self._position = 0


    ## Reads the next character.
    # @return Returns TRUE if a character has been read, otherwise returns FALSE. ##
    def read(self) -> bool:
        if(self.more):
            self._position += 1
            return True

        return False
