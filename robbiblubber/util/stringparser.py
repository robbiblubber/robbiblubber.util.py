
from robbiblubber.util.stringrdr import StringRdr
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class implements a string parser. ##
class StringParser(StringRdr):
    ############################################################################
    # constructor                                                              #
    ############################################################################

    ## Creates a new instance of this class.
    # @param s String.
    # @param position Position. ##
    def __init__(self, s: str = None, position: int = 0):
        super.__init__(self, s, position)

        self._delimiters = [' ', '\r', '\n', '\t']
        self._currentPart = ''
        self._currentDelimiter = ''



    ############################################################################
    # properties                                                               #
    ############################################################################

    ## Gets or sets token delimiters. ##
    @property
    def delitmiters(self) -> []:
        return self._delimiters


    ## Gets or sets token delimiters. ##
    @delitmiters.setter
    def delimiters(self, value: []):
        self._delimiters = value


    ## Gets the current string part. ##
    @property
    def currentPart(self):
        return self._currentPart


    ## Gets the current delimiter. ##
    @property
    def currentDelimiter(self):
        return self._currentDelimiter



    ############################################################################
    # methods                                                                  #
    ############################################################################

    ## Reads to the next occurance of a given token or any of a number of tokens.
    # @param delimiters Token or tokens.
    # @return Returns the part read. ##
    def readPart(self, *delimiters):
        if len(delimiters) == 0: delimiters = self._delimiters

        pos = self._length
        dlm = ''

        for i in delimiters:
            try:
                n = self._string.index(i, self._position)
                if n < pos:
                    pos = n
                    dlm = i
            except(ValueError): pass

        self._currentDelimiter = dlm
        self._currentPart = self._string[self._position, pos]
        self._position = (pos + len(dlm))

        return self._currentPart



    ############################################################################
    # [override] StringRdr                                                     #
    ############################################################################

    ## Gets the preceding character. ##
    @property
    def preceding(self) -> str:
        return self._string[self._position - len(self._currentDelimiter)]
