
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

## This class represents a read line. ##
class _DdpLine:
    ############################################################################
    # constants                                                                #
    ############################################################################

    ## End of file. ##
    EOF = 0

    ## Semicolon as termination sign. ##
    SEMICOLON = 1

    ## Opening bracket. ##
    BRACKET_OPEN = 2

    ## Closing bracket. ##
    BRACKET_CLOSE = 4



    ############################################################################
    # constructor                                                              #
    ############################################################################

    ## Creates a new instance of this class.
    # @param text Text.
    # @param termination Termination. ##
    def __init__(self, text: str, termination: int):
        self.text = text
        self.termination = termination
