
import unittest
from datetime import datetime, timezone, timedelta

from robbiblubber.util.calendarop import CalendarOp
from robbiblubber.util.ddp import Ddp
from robbiblubber.util.mathop import MathOp



## Test class for mathop module. ##
from robbiblubber.util.stringop import StringOp


class UtilTests(unittest.TestCase):
    ## Unit test method for MathOp class. ##
    def test_MathOp(self):
        self.assertEqual(MathOp.power(2, 3), 8)
        self.assertEqual(MathOp.gcf(20, 8), 4)
        self.assertEqual(MathOp.lcm(20, 8), 40)


    ## Unit test method for CalendarOp class. ##
    def test_CalendarOp(self):
        d = datetime(2022, 1, 3, 10, 4, 15, microsecond=324000, tzinfo=timezone(timedelta(seconds=7200)))
        s = CalendarOp.toTimestamp(d)
        self.assertEqual(s, '2022-01-03 10:04:15.324 +02:00')
        self.assertEqual(CalendarOp.parseTimestamp(s), d)


    ## Unit test method for StringOp class. ##
    def test_StringOp(self):
        self.assertEqual('1ho', StringOp.toString(1592, 32))

        self.assertEqual('aaaa', StringOp.before('aaaa/bbb/cc', '/'))
        self.assertEqual('bbb/cc', StringOp.after('aaaa/bbb/cc', '/'))

        self.assertEqual(6, StringOp.matchesFromStart('GOGOGOLIONFOX', 'GOGOGOSUMMER'))
        self.assertEqual(6, StringOp.matchesFromStart('GOGOGOLIONFOX', 'gogogoSummer', False))

        self.assertTrue(StringOp.matches('GOGOGOLIONFOX', ('GOGO*', 'FOX*')))
        self.assertFalse(StringOp.matches('GOGOGOLIONFOX', ('gogo', 'FOX', 'FOX*')))
        self.assertTrue(StringOp.matches('GOGOGOLIONFOX', '*ion*', False))

        self.assertEqual(5, StringOp.strLen(('22', '333', '4444', '55555')))

        self.assertEqual('b*b*b*b*b*', StringOp.replaceAll('bxbXbxbxbX', 'x', '*', False))

        self.assertEqual('bxb*bxbxbX', StringOp.replaceFirst('bxbXbxbxbX', 'X', '*'))
        self.assertEqual('b*bXbxbxbX', StringOp.replaceFirst('bxbXbxbxbX', 'X', '*', False))

        self.assertFalse(StringOp.complies("A tortoise's hat with a feather.", "Aabcdefghijklmnopqrstuvwxyz "))
        self.assertTrue(StringOp.complies("A tortoise's hat with a feather.", "Aabcdefghijklmnopqrstuvwxyz '."))

        self.assertEqual(" '.Aaefhiorstw", StringOp.getChars("A tortoise's hat with a feather."))

        self.assertEqual('PaxPaxdodoRacoon', StringOp.replaceStart('DoDododoRacoon', 'Do', 'Pax'))
        self.assertEqual('RacoondodoPaxPax', StringOp.replaceEnd('RacoondodoDoDo', 'Do', 'Pax'))

        self.assertEqual(' 3, 4, 5, 6', StringOp.snip('1, 2, 3, 4, 5, 6, 7, 8, 9', 2, 6, ','))
        self.assertEqual(' 3/ 4, 5/ 6', StringOp.snip('1/ 2, 3/ 4, 5/ 6, 7/ 8, 9', 2, 6, (',', '/')))

        self.assertEqual('1, 2, 3, 4, 5', StringOp.chain(('1', '2', '3', '4', '5'), ', '))

        self.assertTrue(StringOp.isNumeric('1.22'))
        self.assertTrue(StringOp.isNumeric('-1.22'))
        self.assertTrue(StringOp.isNumeric('+1.22'))
        self.assertFalse(StringOp.isNumeric('/1.22'))
        self.assertFalse(StringOp.isNumeric('1*22'))
        self.assertFalse(StringOp.isNumericInteger('1.22'))
        self.assertFalse(StringOp.isNumericInteger('X?'))
        self.assertTrue(StringOp.isNumeric('-4'))
        self.assertTrue(StringOp.isNumeric('7'))



    ## Unit test method for Ddp class. ##
    def test_Ddp(self):
        ddp = Ddp()

        ddp['test'].value = 'a'

        ddp['Level_1/A'].value = 1
        ddp['Level_1/B'].value = 2

        ddp['Level_1/Level_2/A'].comment = 'Level 1/2/a Comment'
        ddp['Level_1/Level_2/A'].value = 'a'
        ddp['Level_1/Level_2/B'].value = 'a'

        ddp['Gumba'].value = ['Chantal', 'Jacqueline', 'Mireille']
        ddp['Level_1'].value = datetime(1972, 3, 28)

        ddp.value = 'Franz'

        for i in [ ddp, Ddp(ddp.toString()), Ddp.create(ddp.toString(singleLine = True)) ]:
            self.assertEqual(i['test'].value, 'a')
            self.assertEqual(i['/Level_1/B'].getValue(int), 2)
            self.assertEqual(int(i['Level_1']['B']), 2)
            del i['Level_1/B']
            self.assertEqual(int(i['Level_1/B', -1]), -1)
            self.assertEqual(i['Gumba'].getValue([str])[0], 'Chantal')
            self.assertEqual(i.get('Gumba', [str])[0], 'Chantal')
