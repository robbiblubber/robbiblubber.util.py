import unittest

from robbiblubber.util.coding.base62 import Base62
from robbiblubber.util.coding.base64 import Base64
from robbiblubber.util.coding.funnelcake import FunnelCake
from robbiblubber.util.coding.gzip import GZip
from robbiblubber.util.coding.hex import Hex
from robbiblubber.util.coding.md5 import MD5
from robbiblubber.util.coding.sha1 import SHA1
from robbiblubber.util.coding.sha256 import SHA256
from robbiblubber.util.coding.sha512 import SHA512



class CodingTests(unittest.TestCase):
    ## Unit test method for Hex class. ##
    def test_HexEncode(self):
        self.assertEqual(Hex.getInstance().encodeString('Test!'), '5465737421')
        self.assertEqual(Hex.toString('5465737421'), 'Test!')
        self.assertEqual(Hex.getInstance().decode(Hex.getInstance().encode((bytearray([0, 112, 201, 78])))), bytearray([0, 112, 201, 78]))


    ## Unit test method for Base62 class. ##
    def test_Base62(self):
        self.assertEqual(Base62.fromString('All your base are belong to us!'), '3kwzATlmVdpO92e33o7QPmTZbXGgNWXVAOlhoqYPR3')
        self.assertEqual(Base62.toString('QIyC2Cxf'), 'TARDIS')


    ## Unit test method for Base64 class. ##
    def test_Base64(self):
        self.assertEqual(Base64.fromString('Dragonfly'), 'RHJhZ29uZmx5')
        self.assertEqual(Base64.toString('TWF4IFRlc3Q'), 'Max Test')
        self.assertEqual(Base64.fromInt(36600), 'AACO+A')


    ## Unit test method for MD5 class. ##
    def test_MD5(self):
        self.assertEqual(MD5.HEX().hashString('Vindaloo'), '724d8ca5c10b570cde4fa9f9d3347d3f')


    ## Unit test method for GZip. ##
    def test_GZip(self):
        self.assertEqual(GZip.BASE62().encodeString('Hallo'), '3ERvRM01qbyNStiBd3')
        self.assertEqual(GZip.BASE62().decodeToString('3ERvRM01qbyNStiBd3'), 'Hallo')


    ## Unit test method for SHA1. ##
    def test_SHA1(self):
        self.assertEqual(SHA1.HEX().hashString('Vindaloo'), '6d7fa293552e90e3b20d688983b763e6cdaaa525')


    ## Unit test method for SHA256. ##
    def test_SHA256(self):
        self.assertEqual(SHA256.BASE62().hashString('Three blind mice'), 'GqGBIEgx3BOWYF5nXbuCIEjIm6aBqlXRuRFwf8wV9hE')


    ## Unit test method for SHA512. ##
    def test_SHA512(self):
        self.assertEqual(SHA512.BASE64().hashString('12345'), 'NieQminDE4Ggcewn98nKl3Jhgq7Smn3dLlQ1MyLPswq7njpt8qwsIP4jQ2MR1nhWTQyNMFkwV19g4tPQSBhNeQ')


    ## Unit test method for FunnelCake- ##
    def test_FunnelCake(self):
        self.assertEqual(FunnelCake.BASE62().encodeString('Blessed be this funnel cake!'), '32X27pqAcpaxMPAALx33n7Oq4jBM5npXRd0EYj')
        self.assertEqual(FunnelCake.BASE62().decodeToString('32X27pqAcpaxMPAALx33n7Oq4jBM5npXRd0EYj'), 'Blessed be this funnel cake!')
