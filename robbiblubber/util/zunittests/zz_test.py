
import unittest

from robbiblubber.util.zunittests.codingtests import CodingTests
from robbiblubber.util.zunittests.collectiontests import CollectionTests
from robbiblubber.util.zunittests.utiltests import UtilTests



suite = unittest.makeSuite(UtilTests)
suite.addTest(unittest.makeSuite(CodingTests))
suite.addTest(unittest.makeSuite(CollectionTests))

runner = unittest.TextTestRunner()
result = runner.run(suite)

result.printErrors()

