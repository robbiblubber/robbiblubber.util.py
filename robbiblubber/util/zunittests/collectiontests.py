import unittest

from robbiblubber.util.collections.extensibledictionary import ExtensibleDictionary
from robbiblubber.util.collections.extensiblelist import ExtensibleList
from robbiblubber.util.collections.immutabledictionary import ImmutableDictionary
from robbiblubber.util.collections.immutablelist import ImmutableList
from robbiblubber.util.collections.mutabledictionary import MutableDictionary
from robbiblubber.util.collections.mutablelist import MutableList
from robbiblubber.util.collections.tlist import TList


class CollectionTests(unittest.TestCase):
    ## Unit test method for ImmutableList class. ##
    def test_ImmutableList(self):
        m = ImmutableList(('a', 'b', ('1', '2'), ['x', 'y', 'z'], {100, 200}))

        self.assertEqual(m.count, 5)
        self.assertTrue(m.contains('b'))
        self.assertFalse(m.contains('u'))
        self.assertEqual(m[1], 'b')
        self.assertEqual(m.indexOf(('1', '2')), 2)


    ## Unit test method for ExtensibleList class. ##
    def test_ExtensibleList(self):
        m = ExtensibleList(('a', 'b', ('1', '2'), ['x', 'y', 'z'], {100, 200}))

        self.assertEqual(m.count, 5)
        self.assertTrue(m.contains('b'))
        self.assertFalse(m.contains('u'))
        self.assertEqual(m[1], 'b')
        self.assertEqual(m.indexOf(('1', '2')), 2)

        m.add(29)
        m.addRange(('a', 'b'))

        self.assertEqual(m.count, 8)
        self.assertEqual(m[6], 'a')


    ## Unit test method for MutableList class. ##
    def test_MutableList(self):
        m = MutableList(('a', 'b', ('1', '2'), ['x', 'y', 'z'], {100, 200}))

        self.assertEqual(m.count, 5)
        self.assertTrue(m.contains('b'))
        self.assertFalse(m.contains('u'))
        self.assertEqual(m[1], 'b')
        self.assertEqual(m.indexOf(('1', '2')), 2)

        m.add(29)
        m.addRange(('a', 'b'))

        self.assertEqual(m.count, 8)
        self.assertEqual(m[6], 'a')

        m.remove('a');
        self.assertEqual(m.count, 7)
        self.assertEqual(m[0], 'b')
        self.assertEqual(m[6], 'b')

        m[0] = 'x'
        self.assertEqual(m[0], 'x')

        n = TList(m)
        self.assertEqual(m[1], ('1', '2'))

        del m[5]
        self.assertEqual(m[5], 'b')
        self.assertEqual(len(m), 6)


    ## Unit test method for ImmutableDictionary class. ##
    def test_ImmutableDictionary(self):
        m = ImmutableDictionary({'a': 1, 'b': 2, 'c': 3})
        self.assertEqual(m['a'], 1)
        n = 0
        for i in m: n += i
        self.assertEqual(n, 6)
        self.assertEqual(m.itemAt(1), 2)
        self.assertEqual(m.keyAt(2), 'c')
        self.assertEqual(m.keyOf(2), 'b')
        self.assertTrue(m.containsKey('a'))
        self.assertFalse(m.containsKey('u'))
        self.assertTrue(m.contains(2))
        self.assertEqual(m.indexOf(1), 0)
        self.assertEqual(m.indexOfKey('a'), 0)


    ## Unit test method for ExtensibleDictionary class. ##
    def test_ExtensibleDictionary(self):
        m = ExtensibleDictionary({'a': 1, 'b': 2, 'c': 3})
        m.add('d', 4)
        m.addRange({'e': 5, 'f': 6})
        self.assertEqual(m.count, 6)
        n = 0
        for i in m: n += i
        self.assertEqual(n, 21)
        self.assertEqual(m.indexOf(5), 4)
        self.assertEqual(m['e'], 5)
        self.assertRaises(TypeError, m.add, ('a', 7))


    ## Unit test method for MutableDictionary class. ##
    def test_MutableDictionary(self):
        m = MutableDictionary({'a': 1, 'b': 2, 'c': 3})
        m.add('d', 4)
        m.addRange({'e': 5, 'f': 6})
        self.assertEqual(m.count, 6)
        n = 0
        for i in m: n += i
        self.assertEqual(n, 21)
        self.assertEqual(m.indexOf(5), 4)
        self.assertEqual(m['e'], 5)
        self.assertRaises(TypeError, m.add, ('a', 7))

        m.remove('b')
        self.assertEqual(m.itemAt(1), 3)
        m.removeAt(0)
        self.assertEqual(m.itemAt(0), 3)
        self.assertEqual(m.count, 4)
        del m['c']
        self.assertEqual(m.itemAt(0), 4)
        self.assertEqual(len(m), 3)
