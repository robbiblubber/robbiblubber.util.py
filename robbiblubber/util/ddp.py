import os.path
from enum import Enum
import requests

from robbiblubber.util._ftputility import _FtpUtility
from robbiblubber.util.coding.iencoder import IEncoder
from robbiblubber.util.ddpelement import DdpElement
import robbiblubber.util.__version__ as version



## Version. ##
__version__ = version.__version__

class Ddp(DdpElement):
    ############################################################################
    # constants                                                                #
    ############################################################################

    ## String data type. ##
    STR = 0

    ## Integer data type. ##
    INT = 1

    ## Float data type. ##
    FLOAT = 2

    ## Date/time data type. ##
    DATETIME = 4

    ## String array data type. ##
    STR_ARRAY = 128

    ## Int array data type. ##
    INT_ARRAY = 129

    ## Float array data type. ##
    FLOAT_ARRAY = 130

    ## Float array data type. ##
    DATETIME_ARRAY = 132



    ############################################################################
    # constructor                                                              #
    ############################################################################

    ## Creates a new instance of this class.
    # @param s Source string.
    # @param source Source type.
    # @param encoder Encoder.
    # @param user User name.
    # @param password Password. ##
    def __init__(self, s: str = None, source: int = 0, encoder: IEncoder = None, user: str = None, password: str = None):
        super().__init__(None, None)

        if s is not None:
            if isinstance(s, DdpElement):
                s = s.toString()
            elif type(s) != str:
                s = str(s)

        if source == Ddp.Source.UNKNOWN:
            if s is None:
                s = ''
                source = Ddp.Source.TEXT
            elif (s.lower().startswith('http://')) or (s.lower().startswith('http://')):
                source = Ddp.Source.HTTP
            elif s.lower().startswith('ftp://'):
                source = Ddp.Source.FTP
            else:
                try:
                    if os.path.isfile(s):
                        source = Ddp.Source.FILE
                except: pass

                if source == Ddp.Source.UNKNOWN: source = Ddp.Source.TEXT

        self._encoder = encoder
        self._fileName = None

        if source == Ddp.Source.FILE:
            self._fileName = s
            with open(s, 'r') as f:
                self.text = f.read() if (self._encoder is None) else self._encoder.decodeToString(f.read())
        elif source == Ddp.Source.HTTP:
            if user is not None:
                s = requests.get(s).content
            else:
                s = requests.get(s, auth = (user, password)).content

            if self._encoder is not None:
                s = encoder.decodeToString(s)

            self.text = s
        elif source == Ddp.Source.FTP:
            s = _FtpUtility.getFtpContents(s, user, password, None if (encoder is None) else encoder.encoding)

            if encoder is not None:
                s = encoder.decodeToString(s)

            self.text = s
        else:
            self.text = s if (encoder is None) else encoder.decodeToString(s)



    ############################################################################
    # static methods                                                           #
    ############################################################################

    ## Creates a ddp element from a file, network resource, or text.
    # @param s String.
    # @return ddp object. #
    @staticmethod
    def create(s: str):
        return Ddp(s)


    ## Creates a ddp element from a file, network resource, or text.
    # @param s Source string.
    # @param encoder Encoder.
    # @param user User name.
    # @param password Password. ##
    @staticmethod
    def create(s: str = None, encoder: IEncoder = None, user: str = None, password: str = None):
        return Ddp(s, encoder = encoder, user = user, password = password)



    ############################################################################
    # properties                                                               #
    ############################################################################

    ## Gets or sets the file name. ##
    @property
    def fileName(self):
        return self._fileName


    ## Gets or sets the encoder. ##
    @fileName.setter
    def fileName(self):
        return self._fileName


    ## Gets or sets the encoder. ##
    @property
    def encoder(self):
        return self._encoder


    ## Gets or sets the encoder. ##
    @encoder.setter
    def encoder(self):
        return self._encoder



    ############################################################################
    # [override] DdpElement                                                    #
    ############################################################################

    ## Saves the element to a file.
    # @param file File name.
    # @param encoder Encoder. ##
    def save(self, file: str = None, encoder: IEncoder = None):
        if file is None:
            file = self._fileName
        else:
            self._fileName = file

        super().save(file, encoder)



    ############################################################################
    # [class] Source                                                           #
    ############################################################################

    ## This enumeration describes ddp sources. ##
    class Source(Enum):
        ## ddp source is unknown. ##
        UNKNOWN = 0

        ## ddp source is text data. ##
        TEXT = 1

        ## ddp source is a file system file. ##
        FILE = 2

        ## ddp source is a file via HTTP. ##
        HTTP = 3

        ## ddp source is a file via FTP. ##
        FTP = 4
